From f56b5fd8ee7520c0344489bc6e032136a4c80029 Mon Sep 17 00:00:00 2001
From: Ivan Evlogiev <quic_ievlogie@quicinc.com>
Date: Wed, 8 Feb 2023 09:58:58 +0200
Subject: [PATCH 5/7] tensorflow-lite: Improve accuracy for depthwise_conv2d
 with stride

In case stride more than 1 is present, apply padding explicitly before the
depthwise_conv2d node and set padding strategy for depthwise_conv2d node to
NN_PAD_VALID

Signed-off-by: Ivan Evlogiev <quic_ievlogie@quicinc.com>

Change-Id: Id75c526ea1a2274aaf3e98deccd64e46999fb8ea
---
 .../hexagon/builders/conv_2d_builder.cc       | 100 ++++++++++++++----
 1 file changed, 77 insertions(+), 23 deletions(-)

diff --git a/tensorflow/lite/delegates/hexagon/builders/conv_2d_builder.cc b/tensorflow/lite/delegates/hexagon/builders/conv_2d_builder.cc
index 7db75a9f840..5cc59fd23fa 100644
--- a/tensorflow/lite/delegates/hexagon/builders/conv_2d_builder.cc
+++ b/tensorflow/lite/delegates/hexagon/builders/conv_2d_builder.cc
@@ -151,6 +151,9 @@ TfLiteStatus Conv2dOpBuilder::PopulateSubGraph(const TfLiteIntArray* inputs,
     stride_width = conv_params->stride_width;
     padding_type = conv_params->padding;
     activation = conv_params->activation;
+    // We only support dilation for DepthwiseConv.
+    dilation_factors_h_w_.push_back(1);
+    dilation_factors_h_w_.push_back(1);
   } else if (op_node_.op_type == OP_DepthwiseSupernode_8x8p32to8) {
     const TfLiteDepthwiseConvParams* conv_params =
         reinterpret_cast<const TfLiteDepthwiseConvParams*>(builtin_data_);
@@ -163,9 +166,9 @@ TfLiteStatus Conv2dOpBuilder::PopulateSubGraph(const TfLiteIntArray* inputs,
     if (conv_params->dilation_height_factor > 1 ||
         conv_params->dilation_width_factor > 1) {
       is_dilated_depthwise_conv = true;
-      dilation_factors_h_w_.push_back(conv_params->dilation_height_factor);
-      dilation_factors_h_w_.push_back(conv_params->dilation_width_factor);
     }
+    dilation_factors_h_w_.push_back(conv_params->dilation_height_factor);
+    dilation_factors_h_w_.push_back(conv_params->dilation_width_factor);
   }
 
   // Weights tensor
@@ -326,34 +329,85 @@ TfLiteStatus Conv2dOpBuilder::PopulateSubGraph(const TfLiteIntArray* inputs,
         batch_to_space_op->AddOutput(sizeof(float), 4, kScalarShape);
   } else {
     // Standard case.
-    // Padding type.
-    if (padding_type == kTfLitePaddingSame) {
-      SetPaddingType(NN_PAD_SAME);
-    } else if (padding_type == kTfLitePaddingValid) {
-      SetPaddingType(NN_PAD_VALID);
+
+    // In case stride more than 1 is present, apply padding explicitly before the
+    // depthwise_conv2d node and set padding strategy for depthwise_conv2d node
+    // to NN_PAD_VALID
+    TensorID conv_input = graph_builder_->GetHexagonTensorId(inputs->data[0]);
+    tflite::delegates::hexagon::OpBuilder *conv_op = this;
+    if ((1 < stride_height) || (1 < stride_width)) {
+      int input_batch_size, input_height_size, input_width_size, input_depth_size;
+      GetDims(&input_batch_size, &input_height_size, &input_width_size,
+              &input_depth_size, data_tensor.dims);
+      static std::vector<int> paddings;
+      static std::vector<int> crops;
+      ComputeSpaceToBatchParams(
+          input_height_size, input_width_size, weight_shape_[0], weight_shape_[1],
+          dilation_factors_h_w_, padding_type, &paddings, &crops);
+      // Space to batch requires 2x2 padding shape, while pad op requires 3x2
+      //  for each one of height, with and depth
+      paddings.push_back(0);
+      paddings.push_back(0);
+      static std::vector<int> pad_shape = {1, 1, 3, 2};
+      auto* paddings_const = graph_builder_->AddConstNodeWithData(
+          pad_shape.data(),
+          reinterpret_cast<char*>(paddings.data()),
+          paddings.size() * sizeof(paddings[0]));
+
+      auto op_type = op_node_.op_type;
+      SetOpType(OP_QuantizedPad_8);
+      AddInput(conv_input);
+      AddInput(TensorID(data_min_const->GetID(), 0));
+      AddInput(TensorID(data_max_const->GetID(), 0));
+      AddInput(TensorID(paddings_const->GetID(), 0));
+      if (padding_type == kTfLitePaddingSame) {
+        SetPaddingType(NN_PAD_SAME);
+      } else if (padding_type == kTfLitePaddingValid) {
+        SetPaddingType(NN_PAD_VALID);
+      }
+
+      input_height_size += paddings[0] + paddings[1];
+      input_width_size += paddings[2] + paddings[3];
+      conv_input = AddOutput(sizeof(uint8_t), 4,
+              {input_batch_size, input_height_size,
+              input_width_size, input_depth_size});
+      AddOutput(sizeof(float), 4, kScalarShape);
+      AddOutput(sizeof(float), 4, kScalarShape);
+
+      conv_op = graph_builder_->AddNode(GetTFLiteNodeID());
+      conv_op->SetOpType(op_type);
+      conv_op->SetPaddingType(NN_PAD_VALID);
+    } else {
+      // Padding type.
+      if (padding_type == kTfLitePaddingSame) {
+        conv_op->SetPaddingType(NN_PAD_SAME);
+      } else if (padding_type == kTfLitePaddingValid) {
+        conv_op->SetPaddingType(NN_PAD_VALID);
+      }
     }
+
     // Inputs
-    AddInput(graph_builder_->GetHexagonTensorId(inputs->data[0]));
-    AddInput(graph_builder_->GetHexagonTensorId(inputs->data[1]));
-    AddInput(TensorID(data_min_const->GetID(), 0));
-    AddInput(TensorID(data_max_const->GetID(), 0));
-    AddInput(TensorID(weights_min_node_->GetID(), 0));
-    AddInput(TensorID(weights_max_node_->GetID(), 0));
-    AddInput(TensorID(stride_node->GetID(), 0));
-    AddInput(graph_builder_->GetHexagonTensorId(inputs->data[2]));
-    AddInput(TensorID(bias_min_node_->GetID(), 0));
-    AddInput(TensorID(bias_max_node_->GetID(), 0));
-    AddInput(TensorID(conv_output_min_const->GetID(), 0));
-    AddInput(TensorID(conv_output_max_const->GetID(), 0));
+    conv_op->AddInput(conv_input);
+    conv_op->AddInput(graph_builder_->GetHexagonTensorId(inputs->data[1]));
+    conv_op->AddInput(TensorID(data_min_const->GetID(), 0));
+    conv_op->AddInput(TensorID(data_max_const->GetID(), 0));
+    conv_op->AddInput(TensorID(weights_min_node_->GetID(), 0));
+    conv_op->AddInput(TensorID(weights_max_node_->GetID(), 0));
+    conv_op->AddInput(TensorID(stride_node->GetID(), 0));
+    conv_op->AddInput(graph_builder_->GetHexagonTensorId(inputs->data[2]));
+    conv_op->AddInput(TensorID(bias_min_node_->GetID(), 0));
+    conv_op->AddInput(TensorID(bias_max_node_->GetID(), 0));
+    conv_op->AddInput(TensorID(conv_output_min_const->GetID(), 0));
+    conv_op->AddInput(TensorID(conv_output_max_const->GetID(), 0));
     if (per_channel_quant_.channel_scales_node != nullptr) {
-      AddInput(TensorID(per_channel_quant_.channel_scales_node->GetID(), 0));
+      conv_op->AddInput(TensorID(per_channel_quant_.channel_scales_node->GetID(), 0));
     }
     // Outputs
-    output_tensor = AddOutput(sizeof(uint8_t), 4,
+    output_tensor = conv_op->AddOutput(sizeof(uint8_t), 4,
                               {output_batch_size, output_height_size,
                                output_width_size, output_depth_size});
-    output_min_tensor = AddOutput(sizeof(float), 4, kScalarShape);
-    output_max_tensor = AddOutput(sizeof(float), 4, kScalarShape);
+    output_min_tensor = conv_op->AddOutput(sizeof(float), 4, kScalarShape);
+    output_max_tensor = conv_op->AddOutput(sizeof(float), 4, kScalarShape);
   }
 
   // Requantize if activation was not None & the TFLite tensor's min/max is
-- 
2.17.1

