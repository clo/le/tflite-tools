# Copyright (c) 2022-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

$global:TFLITE_DEVICE_INSTALL_PREFIX = ""

# Propagate the correct install path to opkg config
function global:tflite-tools-local-set-opkg-prefix {
    tflite-tools-local-device-command "sed -i '/tflite_install_path/d' /etc/opkg/opkg.conf"
    tflite-tools-local-device-command "echo `"dest tflite_install_path ${TFLITE_DEVICE_INSTALL_PREFIX}`" >> /etc/opkg/opkg.conf"
}

# Propagate errors from adb shell
#   $1 - (mandatory) device command to be executed
function global:tflite-tools-local-device-command {
    param (
        [Parameter (Mandatory = $true)] [string]${CMD}
    )

    adb shell "source /etc/profile.d/tflite-sdk.sh; ${CMD} && echo 0 > /tmp/rc.txt"
    if ($LastExitCode -ne 0) {
        echo "Executing Command ${CMD} failed !!!";
        return 1;
    }

    ${TMP_DIR} = [System.IO.Path]::GetTempPath()

    adb pull /tmp/rc.txt ${TMP_DIR}\rc.txt 2>&1 | Out-null
    if ($LastExitCode -ne 0) {
        echo "${CMD} failed on device !!!";
        return 2;
    }

    adb shell "rm -f /tmp/rc.txt"
    if ($LastExitCode -ne 0) {
        echo "Command adb shell rm -f /tmp/rc.txt failed !!!";
        return 3;
    }
}

# Create Init shell for device
#   which is intended to update PATH and LD_LIBRARY_PATH
# Also deploy it to the device
function global:tflite-tools-local-device-deploy-init-shell {
    param (
        [Parameter (Mandatory = $true)] [string]${TFLITE_DEVICE_INSTALL_PREFIX}
    )
    ${INIT_SHELL_LOCATION_IN_DEVICE}="/etc/profile.d"
    ${INIT_SHELL}="tflite-sdk.sh"
    ${TFLITE_DEVICE_INSTALL_PREFIX_PATH} = "export PATH=${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin"
    ${TFLITE_DEVICE_INSTALL_PREFIX_LD_LIBRARY_PATH} = "export LD_LIBRARY_PATH=${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib"
    adb shell "echo ${TFLITE_DEVICE_INSTALL_PREFIX_PATH}:`'`$`{`PATH`}`'  > /etc/profile.d/tflite-sdk.sh "
    adb shell "echo ${TFLITE_DEVICE_INSTALL_PREFIX_LD_LIBRARY_PATH}:`'`$`{`LD_LIBRARY_PATH`}`' >> /etc/profile.d/tflite-sdk.sh "
    return 0
}

# Sync packages with the device from specified folder
#   $FOLDER - (mandatory) path to the packages to be synced
function global:tflite-tools-local-sync {
    param(
        [Parameter (Mandatory = $true)] [String]$FOLDER
    )

    # Resolve relative/wildcard/absolute path
    $FOLDER = Resolve-Path -Path "$FOLDER"

    ${FORMAT_IPK} = "ipk"
    ${FORMAT_DEB} = "deb"

    pushd ${FOLDER}

    # Set global var
    ${TFLITE_DEVICE_INSTALL_PREFIX} = ( gc tflite-sdk-install-prefix.txt )

    #Push sdk script to device
    tflite-tools-local-device-deploy-init-shell ${TFLITE_DEVICE_INSTALL_PREFIX}
    tflite-tools-local-device-command "mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc"

    if (Test-Path -Path "${FOLDER}\*" -Include *.ipk) {
        tflite-tools-local-set-opkg-prefix
    }

    ${LOG_FILE} = "remote_sync.log"

    if (Test-Path -Path "${FOLDER}\*" -Include local_md5.log) {
        ${LOG_FILE} = "local_md5.log"
    }

    ${DEVICE_LOG_FILE} = "${TFLITE_DEVICE_INSTALL_PREFIX}/etc/device_sync.log"
    ${DEVICE_PULLED_LOG_FILE} = "device_sync.log"

    # Pull device sync log file
    tflite-tools-local-device-command "[ -f ${DEVICE_LOG_FILE} ] || touch ${DEVICE_LOG_FILE}"
    Invoke-Expression "adb pull ${DEVICE_LOG_FILE} ${FOLDER}\${DEVICE_PULLED_LOG_FILE}"

    foreach(${PACKAGE_NAME} in Get-ChildItem ${FOLDER}) {
        ${PACKAGE_FORMAT} = (Get-ChildItem ${PACKAGE_NAME}).Extension
        ${PACKAGE_FORMAT} ="${PACKAGE_FORMAT}".split(".")[1]
        ${PACKAGE_NAME} = (Get-Item ${PACKAGE_NAME} ).Name

        if (${PACKAGE_NAME} -eq "tflite-sdk-install-prefix.txt") {continue;}
        if (${PACKAGE_NAME} -eq "tflite-tools-git-logs.txt") {continue;}
        if (${PACKAGE_NAME} -eq "tflite-sdk.sh") {continue;}
        if (${PACKAGE_NAME} -eq "device_sync.log") {continue;}
        if (${PACKAGE_NAME} -eq "remote_sync.log") {continue;}

        # Get Checksum for current package
        ${CHECKSUM} = (Select-String -SimpleMatch -Pattern "/${PACKAGE_NAME}" -Path "${LOG_FILE}")
        ${CHECKSUM} = "${CHECKSUM}".split(":")[2]
        ${CHECKSUM} = "${CHECKSUM}".split(" ")[0]

        if (${PACKAGE_FORMAT} -eq ${FORMAT_DEB}) {
            Invoke-Expression "adb push ${PACKAGE_NAME} /tmp/"

            if ($LastExitCode -ne 0) {
                popd # ${FOLDER}
                throw "Push package to device failed !!!";
            }

            tflite-tools-local-device-command "dpkg --instdir=${TFLITE_DEVICE_INSTALL_PREFIX} --install --force-all /tmp/${PACKAGE_NAME}"
            if ($LastExitCode -ne 0) {
                Remove-Item ${PACKAGE_NAME}
                tflite-tools-local-device-command "rm -f /tmp/${PACKAGE_NAME}"
                popd # ${FOLDER}
                throw "Install package to device failed !!!";
            }
        }

        if (${PACKAGE_FORMAT} -eq ${FORMAT_IPK}) {
            Invoke-Expression "adb push ${PACKAGE_NAME} /tmp/"

            if ($LastExitCode -ne 0) {
                popd # ${FOLDER}
                throw "Push package to device failed !!!";
            }

            tflite-tools-local-device-command "opkg install -d tflite_install_path --force-reinstall --force-depends --force-overwrite /tmp/${PACKAGE_NAME}"
            if ($LastExitCode -ne 0) {
                Remove-Item ${PACKAGE_NAME}
                tflite-tools-local-device-command "rm -f /tmp/${PACKAGE_NAME}"
                popd # ${FOLDER}
                throw "Install package to device failed !!!";
            }
        }

        (Get-Content ${DEVICE_PULLED_LOG_FILE} | Select-String -SimpleMatch -pattern "/${PACKAGE_NAME}" -notmatch) | Set-Content ${DEVICE_PULLED_LOG_FILE}
        echo "${CHECKSUM} /${PACKAGE_NAME}" | Out-File ${DEVICE_PULLED_LOG_FILE} -Append

        tflite-tools-local-device-command "rm -f /tmp/${PACKAGE_NAME}"
        Remove-Item ${PACKAGE_NAME}
    }

    Invoke-Expression "adb push ${DEVICE_PULLED_LOG_FILE} ${TFLITE_DEVICE_INSTALL_PREFIX}/etc/"
    Remove-Item "${DEVICE_PULLED_LOG_FILE}"
    Remove-Item "${LOG_FILE}"

    popd # ${FOLDER}

    Write-Host "Device sync ready !!!"
}

# Uninstall packages previously installed on the device
#   $FOLDER - (mandatory) path to the folder the packages were synced from
function global:tflite-tools-local-packages-remove {
    param(
        [Parameter (Mandatory = $true)] [String]$FOLDER
    )

    # Resolve relative/wildcard/absolute path
    $FOLDER = Resolve-Path -Path "$FOLDER"

    pushd ${FOLDER}
    $TFLITE_DEVICE_INSTALL_PREFIX = ( gc tflite-sdk-install-prefix.txt )
    popd # ${FOLDER}

    tflite-tools-local-device-command "rm -rf ${TFLITE_DEVICE_INSTALL_PREFIX}"
    if ($LastExitCode -ne 0) {
        throw "Uninstall packages failed !!!";
    }

    Write-Host "Packages uninstalled !!!"
}

# Print help
Write-Host "tflite-tools-local-sync"
Write-Host "    must be invoked to sync packages with the device from specified folder"
Write-Host "tflite-tools-local-packages-remove"
Write-Host "    must be invoked to uninstall packages previously installed on the device"
