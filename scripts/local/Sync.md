# Linux

Script location: /tflite-tools/scripts/local/linux.sh
Sync cmd: tflite-tools-local-sync - Sync packages with the device from specified folder
Uninstall cmd: tflite-tools-local-packages-remove - Uninstall packages previously installed on the device

```bash
source <path to tflite-tools>/scripts/local/linux.sh
tflite-tools-local-sync <folder to sync>
tflite-tools-local-packages-remove <folder with ipks>
```

# Windows

Example for adding adb to powershell path

```powershell
$Env:PATH += ";<path to adb>"
```

Script location: /tflite-tools/scripts/local/linux.sh
Sync cmd: tflite-tools-local-sync - Sync packages with the device from specified folder
Uninstall cmd: tflite-tools-local-packages-remove - Uninstall packages previously installed on the device

```powershell
.\<path to tflite-tools>\scripts\local\win.ps1
tflite-tools-local-sync <folder to sync>
tflite-tools-local-packages-remove <folder with ipks>
```

# Python

### Install necessary pip3 packages
```bash
pip3 install --upgrade pip setuptools wheel
```

### Generate python package (.whl)
```bash
cd </path/to/local/scripts/>
python3 setup_tflite_sync.py bdist_wheel
```

This will generate several directories,
where the package (.whl) will be in "dist" directory

### Install tflite_sync via pip3
```bash
cd ./dist/

pip3 install tflite_sync-1.0-py3-none-any.whl
```

The completion of the above command successfully means that the package has been
successfully installed on the host machine,
and it can be easily invoked by passing the path to the archives and a "command"
The above mentioned "command" can be "install" or "remove"

### For Windows PowerShell

There will be the following warning:
WARNING: The script tflite_sync.exe is installed in
    'C:\Users\CurrentUser\AppData\Local\Packages\PythonSoftwareFoundation.Python.......\LocalCache\local-packages\Python3..\Scripts'
    is not on PATH.

So make it a part of PATH.

```PowerShell
$env:PATH += ";C:\Users\<CurrentUser>\AppData\Local\Packages\PythonSoftwareFoundation.Python.......\LocalCache\local-packages\Python3..\Scripts"
```

### Install packages to the device
```bash
tflite_sync </path/to/tflite/packages/dir/> install
```

### Remove packages from the device
```bash
tflite_sync </path/to/tflite/packages/dir/> remove
```

### Uninstall tflite_sync via pip3
```bash
pip3 uninstall tflite_sync
```

# Note: All python scripts tested on python versions: 3.6.9 and 3.12.1
