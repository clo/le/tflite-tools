#!/bin/bash

# Copyright (c) 2022-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Propagate errors from adb shell
#   $1 - (mandatory) device command to be executed
function tflite-tools-local-device-command () {
    local CMD=$1
    local rc

    adb shell "source /etc/profile.d/tflite-sdk.sh;
        ${CMD} && echo 0 > /data/rc.txt"
    rc=$?
    [ "${rc}" -ne 0 ] && echo "Executing Command ${CMD} failed !!!" && return ${rc}

    local TMP_DIR=$(mktemp -d)

    adb pull /data/rc.txt ${TMP_DIR}/rc.txt 2>&1 > /dev/null
    rc=$?
    adb shell "rm -f /data/rc.txt"
    [ "${rc}" -ne 0 ] && (rm -f ${TMP_DIR}/rc.txt; echo "Command ${CMD} failed !!!") && return ${rc}

    rc=$(cat ${TMP_DIR}/rc.txt)
    rm -f ${TMP_DIR}/rc.txt
    [ "${rc}" == "0" ]                                                                          || \
        {
            echo "Command ${CMD} return code is not 0 !!!";
            return ${rc};
        }

    return 0
}

# Create Init shell for device
#   which is intended to update PATH and LD_LIBRARY_PATH
# Also deploy it to the device
function tflite-tools-device-deploy-init-shell() {
    local TFLITE_DEVICE_INSTALL_PREFIX=$1

    local INIT_SHELL_LOCATION_IN_DEVICE="/etc/profile.d"
    local INIT_SHELL="tflite-sdk.sh"

    touch ${INIT_SHELL}

echo "[[ \""'${PATH}'"\" == *\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin"\"* ]] || \
PATH=\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin:'${PATH}'\"""                       >> ${INIT_SHELL}

echo "[[ \""'${LD_LIBRARY_PATH}'"\" == *\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib"\"* ]] || \
export LD_LIBRARY_PATH=\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib:'${LD_LIBRARY_PATH}'\""" >> ${INIT_SHELL}

    adb push ${INIT_SHELL} ${INIT_SHELL_LOCATION_IN_DEVICE}

    rm ${INIT_SHELL}

    return 0
}

# Propagate the correct install path to opkg config
function tflite-tools-local-set-opkg-prefix () {
    tflite-tools-local-device-command "cat /etc/opkg/opkg.conf | grep \"dest tflite_install_path ${TFLITE_DEVICE_INSTALL_PREFIX}\"" 1>/dev/null || \
        {
            tflite-tools-local-device-command 'sed -i '/tflite_install_path/d' /etc/opkg/opkg.conf'
            tflite-tools-local-device-command "echo \"dest tflite_install_path ${TFLITE_DEVICE_INSTALL_PREFIX}\" >> /etc/opkg/opkg.conf"
        }
}

# Sync packages with the device from specified directory
#   $1 - (mandatory) path to the packages to be synced
function tflite-tools-local-sync() {
    local PACKAGES_PATH=$1

    [ ! -d "${PACKAGES_PATH}" ]                                                                 && \
        {
            echo "Path to directory with packages must be provided as first argument !!!"
            return -1
        }

    PACKAGES_PATH=$(echo ${PACKAGES_PATH}/ | sed 's/\/\//\//g')

    TFLITE_DEVICE_INSTALL_PREFIX=$(cat ${PACKAGES_PATH}/tflite-sdk-install-prefix.txt 2> /dev/null)

    tflite-tools-device-deploy-init-shell ${TFLITE_DEVICE_INSTALL_PREFIX}
    tflite-tools-local-device-command "mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc" || return -2

    [ -n "$(find ${PACKAGES_PATH} -maxdepth 1 -name '*.ipk' -type f -print -quit)" ]            && \
        {
            tflite-tools-local-set-opkg-prefix
        }

    local PKGS=$(ls ${PACKAGES_PATH})

    local LOG_FILE="${PACKAGES_PATH}/remote_sync.log"

    [ -f "${PACKAGES_PATH}/local_md5.log" ] && {
        LOG_FILE="${PACKAGES_PATH}/local_md5.log"
    }

    local DEVICE_LOG_FILE="${TFLITE_DEVICE_INSTALL_PREFIX}/etc/device_sync.log"
    local DEVICE_PULLED_LOG_FILE="${PACKAGES_PATH}/device_sync.log"

    # Pull device sync log file
    tflite-tools-local-device-command "[ -f ${DEVICE_LOG_FILE} ]" 2>&1>/dev/null                || \
        {
            tflite-tools-local-device-command "touch ${DEVICE_LOG_FILE}"
        }

    adb pull ${DEVICE_LOG_FILE} ${PACKAGES_PATH}                                                || \
        {
            echo "Failed to pull device log !!!"
            return -3
        }

    for PACKAGE in ${PKGS[@]}; do
        local FILE="${PACKAGES_PATH}/${PACKAGE}"
        local PACKAGE_FORMAT=$(basename -- "${FILE}")
        PACKAGE_FORMAT="${PACKAGE_FORMAT##*.}"

        # Skip Non-package files
        [ "${PACKAGE}" == "tflite-sdk-install-prefix.txt" ] && continue;
        [ "${PACKAGE}" == "tflite-tools-git-logs.txt" ] && continue;
        [ "${PACKAGE}" == "device_sync.log" ] && continue;
        [ "${PACKAGE}" == "remote_sync.log" ] && continue;
        [ "${PACKAGE}" == "local_md5.log" ] && continue;

        local CHECKSUM=$(grep "/${PACKAGE}" ${LOG_FILE})

        grep -q "${CHECKSUM}" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null                          || \
            {
                [ "${PACKAGE_FORMAT}" == "deb" ]                                                && \
                    {
                        adb push "${FILE}" /tmp/                                                || \
                            {
                                echo "Push package to device failed !!!";
                                return -4;
                            }

                        tflite-tools-local-device-command "dpkg --instdir=${TFLITE_DEVICE_INSTALL_PREFIX} --install --force-all /tmp/${PACKAGE}" || \
                            {
                                tflite-tools-local-device-command "rm /tmp/${PACKAGE}";
                                echo "Install package to device failed !!!";
                                return -5;
                            }
                    }

                [ "${PACKAGE_FORMAT}" == "ipk" ]                                                && \
                    {
                        adb push "${FILE}" /tmp/                                                || \
                            {
                                echo "Push package to device failed !!!";
                                return -6;
                            }

                        tflite-tools-local-device-command "opkg install -d tflite_install_path --force-reinstall --force-depends --force-overwrite /tmp/${PACKAGE}" || \
                            {
                                tflite-tools-local-device-command "rm /tmp/${PACKAGE}";
                                echo "Install package to device failed !!!";
                                return -7;
                            }
                    }
                sed -i "/\/${PACKAGE}/d" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null
                echo "${CHECKSUM} /${PACKAGE}" >> ${DEVICE_PULLED_LOG_FILE}
                tflite-tools-local-device-command "rm -f /tmp/${PACKAGE}"
                rm -f ${FILE}
            }
    done

    adb push ${PACKAGES_PATH}/device_sync.log ${TFLITE_DEVICE_INSTALL_PREFIX}/etc/              || \
        {
            echo "Failed to push updated log to device !!!"
            return -8
        }

    rm -f "${DEVICE_PULLED_LOG_FILE}"
    rm -f "${LOG_FILE}"

    echo "Device synced successfully !!!"

    return 0
}

# Uninstall packages previously installed on the devices
#   $1 - (mandatory) path to the packages to be synced
function tflite-tools-local-packages-remove() {
    local PACKAGES_PATH=$1
    [ ! -d "${PACKAGES_PATH}" ]                                                                 && \
        {
            echo "Path to directory with packages must be provided as first argument !!!"
            return -1
        }

    TFLITE_DEVICE_INSTALL_PREFIX=$(cat ${PACKAGES_PATH}/tflite-sdk-install-prefix.txt 2> /dev/null)

    tflite-tools-local-device-command "rm -rf ${TFLITE_DEVICE_INSTALL_PREFIX}"                  || \
        {
            echo "Device uninstall failed !!!";
            return -2;
        }

    echo "Packages uninstalled successfully !!!"

    return 0
}

# Print help
echo "tflite-tools-local-sync"
echo "    must be invoked to sync packages with the device from specified directory"
echo "tflite-tools-local-packages-remove"
echo "    must be invoked to uninstall packages previously installed on the device"
