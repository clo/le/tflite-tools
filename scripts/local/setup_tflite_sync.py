# Copyright (c) 2023-2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

from setuptools import setup

setup(
    name="tflite_sync",
    version="1.0",
    py_modules=["tflite_sync"],
    entry_points={
        "console_scripts": [
            "tflite_sync = tflite_sync:main"
        ]
    }
)
