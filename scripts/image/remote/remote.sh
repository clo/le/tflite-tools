#!/bin/bash

# Copyright (c) 2021-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Sync compiled package with the remote
#   $1 - (mandatory) path to the package to be synced
function tflite-tools-remote-pkg-sync() {
    local PATH_TO_PACKAGE=$1

    [ -z "${PATH_TO_PACKAGE}" ]                                                                 && \
        print-red "Package name must be provided as first argument !!!"                         && \
        return -1

    [ ! -f "${PATH_TO_PACKAGE}" ]                                                               && \
        print-red "File "${PATH_TO_PACKAGE}" does not exist !!!"                                && \
        return -2

    [ -z "${TFLITE_RSYNC_DST}" ]                                                                && \
        print-red "The  rsync destination must be specified !!!"                                && \
        return -3

    tflite-tools-remote-pull-log

    local LOCAL_LOG_FILE="${TFLITE_PKG_DIR}/local_md5.log"
    local REMOTE_PULLED_LOG_FILE="${TFLITE_PKG_DIR}/remote_sync.log"
    local CHECKSUM=$(grep ${PATH_TO_PACKAGE} ${LOCAL_LOG_FILE} | cut -d ' ' -f1)
    local PACKAGE_NAME=$(basename "${PATH_TO_PACKAGE}")

    [ -z "${CHECKSUM}" ] && {
        CHECKSUM=($(md5sum ${PATH_TO_PACKAGE}))
    }

    grep -q "${CHECKSUM}" ${REMOTE_PULLED_LOG_FILE} 2>&1>/dev/null                              || \
        {
            rsync -a --progress ${PATH_TO_PACKAGE} ${TFLITE_RSYNC_DST}                          || \
                {
                    print-red "rsync package to deploy URL failed !!!";
                    return -4;
                }

            sed -i "/\/${PACKAGE_NAME}/d" ${REMOTE_PULLED_LOG_FILE} 2>&1>/dev/null
            echo "${CHECKSUM} ${PATH_TO_PACKAGE}" >> ${REMOTE_PULLED_LOG_FILE}
        }

    tflite-tools-remote-push-log
    local rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-push-log"
        return $rc
    }

    local TMP_DIR=$(mktemp -d)

    echo "${TFLITE_DEVICE_INSTALL_PREFIX}" >> ${TMP_DIR}/tflite-sdk-install-prefix.txt

    rsync -a --progress ${TMP_DIR}/tflite-sdk-install-prefix.txt ${TFLITE_RSYNC_DST}            || \
        {
            print-red "rsync package to deploy URL failed !!!";
            rm -rf ${TMP_DIR}
            return -5;
        }

    rm -rf ${TMP_DIR}

    return 0
}

# Pull log from remote
function tflite-tools-remote-pull-log() {
    local REMOTE_LOG_FILE="${TFLITE_RSYNC_DST}/remote_sync.log"

    rsync -q ${REMOTE_LOG_FILE} ${TFLITE_PKG_DIR} 2>/dev/null                                   || \
        {
            touch ${TFLITE_PKG_DIR}/remote_sync.log
            rsync -q ${TFLITE_PKG_DIR}/remote_sync.log ${TFLITE_RSYNC_DST}/
        }

    return 0
}

# Push log to remote
function tflite-tools-remote-push-log() {

    rsync -q ${TFLITE_PKG_DIR}/remote_sync.log ${TFLITE_RSYNC_DST}                              || \
        {
            print-red "Failed to sync updated remote log !!!"
            return -1
        }

    return 0
}

# Sync release ipk packages with the remote target
function tflite-tools-remote-sync-ipk-rel-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite_${TFLITE_VERSION}.ipk
}

# Sync release deb packages with the remote target
function tflite-tools-remote-sync-deb-rel-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite_${TFLITE_VERSION}.deb
}

# Sync debug ipk packages with the remote target
function tflite-tools-remote-sync-ipk-dbg-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite-dbg_${TFLITE_VERSION}.ipk
}

# Sync debug deb packages with the remote target
function tflite-tools-remote-sync-deb-dbg-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite-dbg_${TFLITE_VERSION}.deb
}

# Sync dev ipk packages with the remote target
function tflite-tools-remote-sync-ipk-dev-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite-dev_${TFLITE_VERSION}.ipk
}

# Sync dev deb packages with the remote target
function tflite-tools-remote-sync-deb-dev-pkg() {
    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/tflite-dev_${TFLITE_VERSION}.deb
}

print-yellow "tflite-tools-remote-sync-ipk-rel-pkg"
echo "    Sync release ipk packages with the remote target"
print-yellow "tflite-tools-remote-sync-deb-rel-pkg"
echo "    Sync release deb packages with the remote target"
print-yellow "tflite-tools-remote-sync-ipk-dbg-pkg"
echo "    Sync debug ipk packages with the remote target"
print-yellow "tflite-tools-remote-sync-deb-dbg-pkg"
echo "    Sync debug deb packages with the remote target"
print-yellow "tflite-tools-remote-sync-ipk-dev-pkg"
echo "    Sync dev ipk packages with the remote target"
print-yellow "tflite-tools-remote-sync-deb-dev-pkg"
echo "    Sync dev deb packages with the remote target"
