#!/bin/bash

# Copyright (c) 2021-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Check OS
function tflite-tools-platform-check-os() {
    if [ "${TFLITE_OS}" == "la" ]; then
        print-red "Command not supported for LA!"
        return -1
    fi

    return 0
}

# Check wheter invoked from docker build image
function tflite-tools-platform-check-image-build() {
    local CHECK_BUILD=`ps -a | grep "com.docker.cli"`
    local rc=$?

    [ ${rc} -eq 0 ] && return 0 || return 1
}
