#!/bin/bash

# Copyright (c) 2021-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Propagate errors from adb shell
#   $1 - (mandatory) cmd to be executed
function tflite-tools-device-command () {
    local CMD=$1
    local rc

    adb shell "source /etc/profile.d/tflite-sdk.sh;
            ${CMD} && echo 0 > /data/rc.txt"
    rc=$?
    [ $rc -ne 0 ] && print-red "Executing Command ${CMD} failed !!!" && return $rc

    local TMP_DIR=`mktemp -d`

    adb pull /data/rc.txt /${TMP_DIR}/rc.txt 2>&1 > /dev/null
    rc=$?
    adb shell "rm -f /data/rc.txt"
    [ $rc -ne 0 ] && (rm -f /${TMP_DIR}/rc.txt; print-red "Command ${CMD} failed !!!") && return $rc

    rc=`cat /${TMP_DIR}/rc.txt`
    rm -f /${TMP_DIR}/rc.txt
    [ ! "$rc" == "0" ] && print-red "Command ${CMD} return code is not 0 !!!" && return $rc

    return 0
}

# Prepare device
function tflite-tools-device-prepare() {
    local rc

    echo "Waiting for device"
    adb wait-for-device root
    rc=$?
    [ $rc -ne 0 ] && print-red "adb root failed !!!" && return -1

    adb wait-for-device remount wait-for-device
    rc=$?
    [ $rc -ne 0 ] && print-red "adb remount failed !!!" && return -2

    tflite-tools-device-deploy-init-shell
    rc=$?
    [ $rc -ne 0 ] && print-red "deploy init shell to device failed !!!" && return $rc

    tflite-tools-device-command "mount -o remount,rw / > /dev/null"
    rc=$?
    [ $rc -ne 0 ] && print-red "adb file system remount failed !!!" && return -3

    tflite-tools-device-command "! command -v setenforce || setenforce 0"
    rc=$?
    [ $rc -ne 0 ] && print-red "adb disable SE linux failed !!!" && return -4

    print-green "Device prepared successfully !!!"
}

# Incremental deploy folder to the LE device
#   $1 - (mandatory) folder to deploy to the device
function tflite-tools-device-deploy-folder-le() {
    [ $# -eq 0 ]                                                                                && \
        {
            print-red "Error: At least one input argument is required to specify which folder to deploy to the device"
            return -1
        }

    local DEPLOY_FOLDER=$1/usr
    local CHECK_TIME
    [ -f ${TFLITE_DEPLOY_DIR}/sync_timestamp ] && CHECK_TIME="-anewer ${TFLITE_DEPLOY_DIR}/sync_timestamp"

    pushd ${DEPLOY_FOLDER} 1>/dev/null                                                          && \
        local TO_PUSH=`find . ${CHECK_TIME}`                                                    && \
        [ ${#TO_PUSH} -ne 0 ]                                                                   && \
        tar -cf ${TFLITE_DEPLOY_DIR}/sync.tar ${TO_PUSH}                                        && \
        adb push  ${TFLITE_DEPLOY_DIR}/sync.tar /data                                           && \
        tflite-tools-device-command "tar -xf /data/sync.tar -C ${TFLITE_DEVICE_INSTALL_PREFIX}/usr/" && \
        touch ${TFLITE_DEPLOY_DIR}/sync_timestamp                                               || \
            {
                print-red "Deploy folder to LE device failed: adb push failed !!!"
                return -2
            }

    tflite-tools-device-command "rm -f /data/sync.tar"
    popd 1>/dev/null
    rm -f ${TFLITE_DEPLOY_DIR}/sync.tar

    print-green "Device update ready !"
}

# Incremental deploy folder to the LA device
#   $1 - (mandatory) folder to deploy to the device
function tflite-tools-device-deploy-folder-la() {
    [ $# -eq 0 ]                                                                                && \
        {
            print-red "Error: At least one input argument is required to specify which folder to deploy to the device"
            return -1
        }

    local DEPLOY_FOLDER=$1
    export ANDROID_PRODUCT_OUT=${DEPLOY_FOLDER}
    adb sync vendor                                                                             || \
        {
            print-red "Deploy folder to LA device failed: adb sync failed !!!"
            return -2
        }

    print-green "Device update ready !"
}

# Incremental deploy folder to the LU device
#   $1 - (mandatory) folder to deploy to the device
function tflite-tools-device-deploy-folder-lu() {
    tflite-tools-device-deploy-folder-le $1
}

# Deploy Release variant to the device
function tflite-tools-device-deploy() {
    tflite-tools-device-deploy-folder-${TFLITE_OS} ${TFLITE_DEPLOY_DIR}
}

# Deploy Debug variant with debug symbols to the device
function tflite-tools-device-deploy-dbg() {
    tflite-tools-device-deploy-folder-${TFLITE_OS} ${TFLITE_DEPLOY_DBG_DIR}
}

# Deploy and install ipk package to the device
#   $1 - (mandatory) path to ipk file
function tflite-tools-device-pkg-deploy() {
    tflite-tools-platform-check-os || return -1

    local PATH_TO_PACKAGE=$1
    [ -z "${PATH_TO_PACKAGE}" ]                                                                 && \
        print-red "Package name must be provided as first argument !!!"                         && \
        return -2

    [ ! -f "${PATH_TO_PACKAGE}" ]                                                               && \
        print-red "File "${PATH_TO_PACKAGE}" does not exist !!!"                                && \
        return -3

    tflite-tools-device-command "mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc"
    local rc=$?
    [ $rc -ne 0 ] && print-red "FAILED: mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc" && return $rc

    tflite-tools-device-pull-log
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-pull-log"
        return $rc
    }

    local LOCAL_LOG_FILE="${TFLITE_PKG_DIR}/local_md5.log"
    local DEVICE_PULLED_LOG_FILE="${TFLITE_PKG_DIR}/device_sync.log"
    local CHECKSUM=$(grep "${PATH_TO_PACKAGE}" ${LOCAL_LOG_FILE} | cut -d ' ' -f1)
    local PACKAGE_NAME=$(basename "${PATH_TO_PACKAGE}")

    grep -q "${CHECKSUM}" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null                              || \
    {
        adb push "${PATH_TO_PACKAGE}" /tmp/                                                     || \
            {
                print-red "Push package to device /tmp directory failed !!!";
                return -4;
            }

        tflite-tools-device-command "opkg --offline-root=${TFLITE_DEVICE_INSTALL_PREFIX} --force-depends --force-reinstall --force-overwrite install /tmp/${PACKAGE_NAME}" || \
            {
                tflite-tools-device-command "rm /tmp/${PACKAGE_NAME}";
                print-red "Install package to device failed !!!";
                return -5;
            }

        tflite-tools-device-command "rm /tmp/${PACKAGE_NAME}"                                   || \
            {
                print-red "Remove package from device /tmp directory failed !!!";
                return -6;
            }

        sed -i "/\/${PACKAGE_NAME}/d" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null
        echo "${CHECKSUM} ${PATH_TO_PACKAGE}" >> ${DEVICE_PULLED_LOG_FILE}
    }

    tflite-tools-device-push-log
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-pull-log"
        return $rc
    }

    return 0
}

# Deploy and install deb package to the device
#   $1 - (mandatory) path to deb file
function tflite-tools-device-deb-deploy() {
    tflite-tools-platform-check-os || return -1

    local PATH_TO_PACKAGE=$1
    [ -z "${PATH_TO_PACKAGE}" ]                                                                 && \
        print-red "Package name must be provided as first argument !!!"                         && \
        return -2

    [ ! -f "${PATH_TO_PACKAGE}" ]                                                               && \
        print-red "File "${PATH_TO_PACKAGE}" does not exist !!!"                                && \
        return -3

    tflite-tools-device-command "mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc"
    local rc=$?
    [ $rc -ne 0 ] && print-red "FAILED: mkdir -p ${TFLITE_DEVICE_INSTALL_PREFIX}/etc" && return $rc

    tflite-tools-device-pull-log
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-pull-log"
        return $rc
    }

    local LOCAL_LOG_FILE="${TFLITE_PKG_DIR}/local_md5.log"
    local DEVICE_PULLED_LOG_FILE="${TFLITE_PKG_DIR}/device_sync.log"
    local CHECKSUM=$(grep "${PATH_TO_PACKAGE}" ${LOCAL_LOG_FILE} | cut -d ' ' -f1)
    local PACKAGE_NAME=$(basename "${PATH_TO_PACKAGE}")

    grep -q "${CHECKSUM}" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null                              || \
    {
        adb push "${PATH_TO_PACKAGE}" /tmp/                                                     || \
            {
                print-red "Push package to device /tmp directory failed !!!";
                return -4;
            }

        tflite-tools-device-command "dpkg --instdir=${TFLITE_DEVICE_INSTALL_PREFIX} --install --force-all /tmp/${PACKAGE_NAME}" || \
            {
                tflite-tools-device-command "rm /tmp/${PACKAGE_NAME}";
                print-red "Install package to device failed !!!";
                return -5;
            }

        tflite-tools-device-command "rm /tmp/${PACKAGE_NAME}"                                   || \
            {
                print-red "Remove package from device /tmp directory failed !!!";
                return -6;
            }

        sed -i "/\/${PACKAGE_NAME}/d" ${DEVICE_PULLED_LOG_FILE} 2>&1>/dev/null
        echo "${CHECKSUM} ${PATH_TO_PACKAGE}" >> ${DEVICE_PULLED_LOG_FILE}
    }

    tflite-tools-device-push-log
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-pull-log"
        return $rc
    }

    return 0
}

# Remove installed packages from the device
function tflite-tools-device-packages-remove() {
    local rc

    tflite-tools-device-command "rm -rf ${TFLITE_DEVICE_INSTALL_PREFIX}"
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-command "rm ${TFLITE_DEVICE_INSTALL_PREFIX}""
        return $rc
    }

    print-green "Package removed successfully !!!"

    return 0
}

# Create Init shell for device
#   which is intended to update PATH and LD_LIBRARY_PATH
# Also deploy it to the device
function tflite-tools-device-deploy-init-shell() {
    local INIT_SHELL_LOCATION_IN_DEVICE="/etc/profile.d"
    local INIT_SHELL="tflite-sdk.sh"

    touch ${INIT_SHELL}

echo "[[ \""'${PATH}'"\" == *\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin"\"* ]] || \
PATH=\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin:'${PATH}'\"""                       >> ${INIT_SHELL}

echo "[[ \""'${LD_LIBRARY_PATH}'"\" == *\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib"\"* ]] || \
export LD_LIBRARY_PATH=\""${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib:'${LD_LIBRARY_PATH}'\"""               >> ${INIT_SHELL}

    adb push ${INIT_SHELL} ${INIT_SHELL_LOCATION_IN_DEVICE}

    rm ${INIT_SHELL}

    return 0
}

# Pull log from device
function tflite-tools-device-pull-log() {
    local DEVICE_LOG_FILE="${TFLITE_DEVICE_INSTALL_PREFIX}/etc/device_sync.log"

    tflite-tools-device-command "[ -f ${DEVICE_LOG_FILE} ]" 2>/dev/null                         || \
        {
            tflite-tools-device-command "touch ${DEVICE_LOG_FILE}"
        }

    adb pull ${DEVICE_LOG_FILE} ${TFLITE_PKG_DIR}/                                              || \
        {
            print-red "Failed to pull device log !!!"
            return -1
        }

    return 0
}

# Push log to device
function tflite-tools-device-push-log() {
    adb push ${TFLITE_PKG_DIR}/device_sync.log ${TFLITE_DEVICE_INSTALL_PREFIX}/etc/             || \
        {
            print-red "Failed to push updated log to device !!!"
            return -1
        }

    return 0
}

print-green "tflite-tools-device-prepare"
echo "    Prepare device"
print-green "tflite-tools-device-deploy"
echo "    Deploy Release variant to the device"
print-green "tflite-tools-device-deploy-dbg"
echo "    Deploy Debug variant with debug symbols to the device"
print-green "tflite-tools-device-pkg-deploy"
echo "    Deploy and install ipk package to the device"
print-green "tflite-tools-device-deb-deploy"
echo "    Deploy and install deb package to the device"
print-green "tflite-tools-device-packages-remove"
echo "    Remove installed packages from the device"
print-red "The following variables need to be exported in the device, before running apps"
echo "    \${PATH} and \${LD_LIBRARY_PATH}"
print-red "Binaries will be installed on the device in ${TFLITE_DEVICE_INSTALL_PREFIX}/usr/bin"
print-red "Libraries will be installed on the device in ${TFLITE_DEVICE_INSTALL_PREFIX}/usr/lib"
