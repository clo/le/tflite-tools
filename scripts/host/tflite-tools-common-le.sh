#!/bin/bash

# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Install SDK
function tflite-tools-install-sdk() {
    local rc

    [ ! -d ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE} ] && {
        echo "Installing ${SDK_NAME} SDK..."

        chmod a+r ${SDK_PATH}/${SDK_NAME}
        umask 022
        ${SDK_PATH}/${SDK_NAME} -y -d ${TFLITE_SDK_BASE_DIR}/${SDK_NAME}

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}
            return $rc
        }

        mkdir -p ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}

    } || echo "The ${SDK_NAME} SDK: Already installed"

    local SDK_JSON=${SDK_NAME%.*}
    SDK_JSON="${SDK_JSON}.testdata.json"

    local ENV_SCRIPT_VAR_SUFFIX=""
    cat ${SDK_PATH}/${SDK_JSON} | grep env_setup_script_llvm > /dev/null && ENV_SCRIPT_VAR_SUFFIX="_llvm"

    local ENV_SCRIPT_VAR="env_setup_script${ENV_SCRIPT_VAR_SUFFIX}"

    export TFLITE_TARGET_SYS=`cat ${SDK_PATH}/${SDK_JSON} | jq '.TARGET_SYS' | tr -d '"'`

    local POST_SDK_ENV_SETUP_SCRIPT=$(cat ${SDK_PATH}/${SDK_JSON} | grep "${ENV_SCRIPT_VAR}")

    local SDK_ENV_SETUP_SCRIPT=${POST_SDK_ENV_SETUP_SCRIPT#*$ENV_SCRIPT_VAR}

    SDK_ENV_SETUP_SCRIPT=$(echo ${SDK_ENV_SETUP_SCRIPT} | cut -d '\' -f 2 | cut -d '/' -f 2)

    TFLITE_SDK_BASE_DIR=${TFLITE_SDK_BASE_DIR}/${SDK_NAME}

    export TFLITE_SDK_SETUP="source ${TFLITE_SDK_BASE_DIR}/${SDK_ENV_SETUP_SCRIPT}"

    return 0
}

# Install Proto Buffer for LE
function tflite-tools-install-protoc-for-le() {
    local rc

    local PROTO_BUFF_VERSION="3.17.3"

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION} ] && {
        echo "Installing Proto Buffer ${PROTO_BUFF_VERSION} for LE..."

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt

        local TF_LITE_NUM_CPU
        tflite-tools-get-number-of-threads TF_LITE_NUM_CPU

        (wget --quiet https://github.com/protocolbuffers/protobuf/releases/download/v${PROTO_BUFF_VERSION}/protobuf-cpp-${PROTO_BUFF_VERSION}.tar.gz && \
            tar xf protobuf-cpp-${PROTO_BUFF_VERSION}.tar.gz                                                        && \
            cd protobuf-${PROTO_BUFF_VERSION}                                                                       && \
            mkdir x86_64_build                                                                                      && \
            cd x86_64_build/                                                                                        && \
            ../configure --disable-shared --prefix=${PWD}/x86_64_pb_install                                         && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            ${TFLITE_SDK_SETUP}                                                                                     && \
            cp -v x86_64_pb_install/bin/protoc ${OECORE_NATIVE_SYSROOT}/usr/bin/                                       \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cp -v x86_64_pb_install/lib/libprotobuf.a ${OECORE_NATIVE_SYSROOT}/usr/lib/                                \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cp -v -r x86_64_pb_install/include/google ${OECORE_NATIVE_SYSROOT}/usr/include                             \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cd ../                                                                                                  && \
            mkdir arm64_build                                                                                       && \
            cd arm64_build/                                                                                         && \
            ../configure --disable-shared --with-pic --prefix=${PWD}/arm64_pb_install --host=aarch64-linux --with-protoc=${OECORE_NATIVE_SYSROOT}/usr/bin/protoc ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            cp -v arm64_pb_install/lib/libprotobuf.a ${SDKTARGETSYSROOT}/usr/lib/                                      \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            mkdir -p ${SDKTARGETSYSROOT}/usr/include                                                                && \
            cp -v -r arm64_pb_install/include/* ${SDKTARGETSYSROOT}/usr/include                                        \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cd ../../                                                                                               && \
            rm -rf protobuf*)

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt
            rm -rf protobuf*
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}

    } || echo "Proto Buffer ${PROTO_BUFF_VERSION} for LE compiled by ${SDK_TO_BUILD_TFLITE}: Already installed"

    return 0
}

# Install JPEG Library for LE
function tflite-tools-install-jpeg-turbo-for-le() {
    local rc

    local JPEG_VERSION="2.0.90"

    local TFLITE_TARGET=( $(${TFLITE_SDK_SETUP} 2>/dev/null ; echo ${TARGET_PREFIX}) )

    TFLITE_TARGET=$(echo ${TFLITE_TARGET} | rev | cut -c2- | rev)

    echo ${TFLITE_TARGET} | grep 'gnu'
    rc=$?

    [ $rc -ne 0 ] && {
        TFLITE_TARGET=""
    }

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library ] && {
        echo "Installing JPEG Library ${JPEG_VERSION} for LE..."

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt

        local TF_LITE_NUM_CPU
        tflite-tools-get-number-of-threads TF_LITE_NUM_CPU

        (${TFLITE_SDK_SETUP}                                                                                        && \
            wget --quiet --output-document libjpeg-${JPEG_VERSION}.tar.xz https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/${JPEG_VERSION}.tar.gz && \
            tar xf libjpeg-${JPEG_VERSION}.tar.xz                                                                   && \
            cd libjpeg-turbo-${JPEG_VERSION}/                                                                       && \
            cmake -DCMAKE_TOOLCHAIN_FILE=${OE_CMAKE_TOOLCHAIN_FILE}                                                    \
                -DCMAKE_INSTALL_DEFAULT_PREFIX=${PWD}/install_dir                                                      \
                -DCMAKE_POSITION_INDEPENDENT_CODE=ON                                                                   \
                -DWITH_TURBOJPEG=FALSE                                                                                 \
                -DENABLE_SHARED=FALSE .                                                                             && \
            make -j${TF_LITE_NUM_CPU}                                                                               && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            cp -v ./install_dir/lib64/libjpeg.a* ${SDKTARGETSYSROOT}/usr/lib/${TFLITE_TARGET}                          \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt                  && \
            cp -v ./install_dir/include/* ${SDKTARGETSYSROOT}/usr/include/                                             \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt                  && \
            cd ../                                                                                                  && \
            rm -rf libjpeg*)

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt
            rm -rf libjpeg*
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library

    } || echo "JPEG Library ${JPEG_VERSION} for LE compiled by ${SDK_TO_BUILD_TFLITE}: Already installed"

    return 0
}

# Uninstall SDK and also JPEG Turbo and Proto Buffer, because there is dependency
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-sdk() {
    local rc

    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local SDK_TO_REMOVE=${BASE_DIR}/${COMMON_DIR}/sdk
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_SDK=""

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK"
        return $rc
    }

    [ -d ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK} ]                                                                && \
    [ -d ${SDK_TO_REMOVE} ] && {

        pushd ${SDK_TO_REMOVE} 1>/dev/null

        tflite-tools-uninstall-protoc-for-le
        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-uninstall-protoc-for-le"
            popd 1>/dev/null
            return $rc
        }

        tflite-tools-uninstall-jpeg-turbo-for-le
        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-uninstall-jpeg-turbo-for-le"
            popd 1>/dev/null
            return $rc
        }

        rm -rf ${SDK_NAME}
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}

        popd 1>/dev/null
    } || echo "SDK ${SDK_NAME}: Already removed"

    return 0
}

# Uninstall Proto Buffer for LE
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-protoc-for-le() {
    local rc

    local PROTO_BUFF_VERSION="3.17.3"

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_SDK=""

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK"
        return $rc
    }

    [ -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/protobuffer_${PROTO_BUFF_VERSION} ] && {
        [ ! -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt ] && {
            print-red "Proto Buffer ${PROTO_BUFF_VERSION} for LE: Can not remove, try to install Proto buffer"
            return -1
        }

        local REMOVE_INFO=`cat ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt | rev | cut -d "'" -f 2 | rev`

        echo "${REMOVE_INFO}" | tr '\n' ' ' | xargs rm -rf

        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/protobuffer_${PROTO_BUFF_VERSION}
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt
    } || echo "Proto Buffer ${PROTO_BUFF_VERSION} for LE: Already removed"

    return 0
}

# Uninstall JPEG Turbo for LE
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-jpeg-turbo-for-le() {
    local rc

    local JPEG_VERSION="2.0.90"

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_SDK=""

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK"
        return $rc
    }

    [ -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/jpeg_${JPEG_VERSION}_library ] && {
        [ ! -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/jpeg_${JPEG_VERSION}_library_backup.txt ] && {
            print-red "JPEG Library ${JPEG_VERSION} for LE: Can not remove, try to install JPEG Library"
            return -1
        }

        local REMOVE_INFO=`cat ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/jpeg_${JPEG_VERSION}_library_backup.txt | rev | cut -d "'" -f 2 | rev`

        echo "${REMOVE_INFO}" | tr '\n' ' ' | xargs rm -rf

        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/jpeg_${JPEG_VERSION}_library
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/jpeg_${JPEG_VERSION}_library_backup.txt
    } || echo "JPEG Library ${JPEG_VERSION} for LE: Already removed"

    return 0
}
