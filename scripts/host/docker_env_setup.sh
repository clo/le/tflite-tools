#!/bin/bash

# Copyright (c) 2021-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

echo "Docker build environment setup"
echo "=============================="

function print-red() {
    tput setaf 1 2>/dev/null
    echo $@
    tput sgr0 2>/dev/null
    true
}

function print-green() {
    tput setaf 2 2>/dev/null
    echo $@
    tput sgr0 2>/dev/null
    true
}

function print-yellow() {
    tput setaf 3 2>/dev/null
    echo $@
    tput sgr0 2>/dev/null
    true
}

function print-blue() {
    tput setaf 4 2>/dev/null
    echo $@
    tput sgr0 2>/dev/null
    true
}

# Propagate errors from adb shell
#   $1 - (mandatory) cmd to be executed
function tflite-tools-device-command () {
    local CMD=$1
    local rc

    adb shell "${CMD} && echo 0 > /data/rc.txt"
    rc=$?
    [ $rc -ne 0 ] && print-red "Executing Command ${CMD} failed !!!" && return $rc

    adb pull /data/rc.txt /tmp/rc.txt 2>&1 > /dev/null
    rc=$?
    adb shell "rm -f /data/rc.txt"
    [ $rc -ne 0 ] && (rm -f /tmp/rc.txt; print-red "Command ${CMD} failed !!!") && return $rc

    rc=`cat /tmp/rc.txt`
    rm -f /tmp/rc.txt
    [ $rc -ne 0 ] && print-red "Command ${CMD} return code is not 0 !!!" && return $rc

    return $rc
}

# Parse json configuraiton
#   $1 - (mandatory) path to target config json
#   $2 - /output/ (mandatory) image
#   $3 - /output/ (mandatory) device os
#   $4 - /output/ (mandatory) additional tag
#   $5 - /output/ (mandatory) tflite version
#   $6 - /output/ (mandatory) container name
#   $7 - /output/ (optional) rsync destination
#   $8 - /output/ (mandatory for LE) full path to sdk file
#   $9 - /output/ (mandatory) environment setup script of sdk
#   $10 - /output/ (mandatory) to enable/disable hexagon delegate option
#   $11 - /output/ (mandatory) to enable/disable gpu delegate option
#   $12 - /output/ (mandatory) to enable/disable xnnpack delegate option
#   $13 - /output/ (mandatory) to specify device install prefix
#   $14 - /output/ (mandatory) to specify target sys
function tflite-tools-host-parse-json() {
    local PATH_TO_CONFIG_JSON=$1
    local -n OUT_IMAGE=$2
    local -n OUT_DEVICE_OS=$3
    local -n OUT_ADDITIONAL_TAG=$4
    local -n OUT_TFLITE_VERSION=$5
    local -n OUT_CONTAINER_NAME=$6
    local -n OUT_TFLITE_RSYNC_DST=$7
    local -n OUT_SDK_FULL_PATH=$8
    local -n OUT_SDK_ENV_SETUP_SCRIPT=$9
    local -n OUT_HEXAGON_DELEGATE=${10}
    local -n OUT_GPU_DELEGATE=${11}
    local -n OUT_XNNPACK_DELEGATE=${12}
    local -n OUT_TFLITE_DEVICE_INSTALL_PREFIX=${13}
    local -n OUT_TFLITE_TARGET_SYS=${14}

    [ ! -f "${PATH_TO_CONFIG_JSON}" ]                                                                               && \
        print-red "Path to config json must be provided as first argument !!!"                                      && \
        return -1

    local BUFFER=`cat ${PATH_TO_CONFIG_JSON}`

    OUT_IMAGE=`echo ${BUFFER} | jq '.Image' | tr -d '"'`
    [ ! "${OUT_IMAGE}" == "tflite-tools-builder" ] && [ ! "${OUT_IMAGE}" == "tflite-tools-builder-dev" ]            && \
        print-red "Reconfigure Image attribute with correct data in targets/.json file !!!"                         && \
        print-yellow "Avaliable Images are tflite-tools-builder and tflite-tools-builder-dev."                      && \
        return -2

    OUT_DEVICE_OS=`echo ${BUFFER} |  jq '.Device_OS' | tr -d '"'`
    [ ! "${OUT_DEVICE_OS}" == "le" ] && [ ! "${OUT_DEVICE_OS}" == "la" ] && [ ! "${OUT_DEVICE_OS}" == "lu" ]        && \
        print-red "Reconfigure Device_OS attribute with correct data in targets/.json file !!!"                     && \
        print-yellow "Avaliable OS are le, lu and la."                                                              && \
        return -3

    OUT_ADDITIONAL_TAG=`echo ${BUFFER} |  jq '.Additional_tag' | tr -d '"'`
    [[ ! -z "${OUT_ADDITIONAL_TAG}" ]] && OUT_ADDITIONAL_TAG="-${OUT_ADDITIONAL_TAG}"

    OUT_TFLITE_VERSION=`echo ${BUFFER} | jq '.TFLite_Version' | tr -d '"'`
    [ ! "${OUT_TFLITE_VERSION}" == "2.15.0" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.14.1" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.13.1" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.12.1" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.11.1" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.10.1" ]                                                                       && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.8.0" ]                                                                        && \
    [ ! "${OUT_TFLITE_VERSION}" == "2.6.0" ]                                                                        && \
        print-red "Reconfigure Version attribute with correct data in targets/.json file !!!"                       && \
        print-yellow "Avaliable versions are:"                                                                      && \
        print-yellow "2.15.0, 2.14.1, 2.13.1, 2.12.1, 2.11.1, 2.10.1, 2.8.0 and 2.6.0 !!!"                          && \
        return -4

    OUT_TFLITE_RSYNC_DST=`echo ${BUFFER} | jq '.TFLite_rsync_destination' | tr -d '"'`

    local SDK_PATH=`echo ${BUFFER} | jq '.SDK_path' | tr -d '"'`
    local SDK_FILE=`echo ${BUFFER} | jq '.SDK_shell_file' | tr -d '"'`
    OUT_SDK_FULL_PATH=${SDK_PATH}/${SDK_FILE}

    [ "${OUT_DEVICE_OS}" == "le" ] || [ "${OUT_DEVICE_OS}" == "lu" ] && [ ! -f ${OUT_SDK_FULL_PATH} ]               && \
        print-red "SDK path and file are not specified !!!"                                                         && \
        return -5

    local SDK_JSON=${SDK_FILE%.*}
    SDK_JSON="${SDK_JSON}.testdata.json"

    local ENV_SCRIPT_VAR_SUFFIX=""
    cat ${SDK_PATH}/${SDK_JSON} | grep env_setup_script_llvm > /dev/null && ENV_SCRIPT_VAR_SUFFIX="_llvm"

    local ENV_SCRIPT_VAR="env_setup_script${ENV_SCRIPT_VAR_SUFFIX}"

    OUT_TFLITE_TARGET_SYS=`cat ${SDK_PATH}/${SDK_JSON} | jq '.TARGET_SYS' | tr -d '"'`

    local POST_SDK_ENV_SETUP_SCRIPT=$(cat ${SDK_PATH}/${SDK_JSON} | grep "${ENV_SCRIPT_VAR}")

    OUT_SDK_ENV_SETUP_SCRIPT=${POST_SDK_ENV_SETUP_SCRIPT#*$ENV_SCRIPT_VAR}

    OUT_SDK_ENV_SETUP_SCRIPT=$(echo ${OUT_SDK_ENV_SETUP_SCRIPT} | cut -d '\' -f 2 | cut -d '/' -f 2)

    OUT_CONTAINER_NAME="${OUT_DEVICE_OS}-${OUT_IMAGE}-${OUT_TFLITE_VERSION}${OUT_ADDITIONAL_TAG}"

    local DELEGATE_BUFFER=`echo ${BUFFER} | jq '.Delegates'`

    OUT_HEXAGON_DELEGATE=`echo ${DELEGATE_BUFFER} | jq '.Hexagon_delegate' | tr -d '"'`
    OUT_GPU_DELEGATE=`echo ${DELEGATE_BUFFER} | jq '.Gpu_delegate' | tr -d '"'`
    OUT_XNNPACK_DELEGATE=`echo ${DELEGATE_BUFFER} | jq '.Xnnpack_delegate' | tr -d '"'`

    OUT_TFLITE_DEVICE_INSTALL_PREFIX=`echo ${BUFFER} | jq '.Device_install_prefix' | tr -d '"'`

    OUT_TFLITE_DEVICE_INSTALL_PREFIX=`echo ${OUT_TFLITE_DEVICE_INSTALL_PREFIX} | tr -s '/'`

    [ -z "${OUT_TFLITE_DEVICE_INSTALL_PREFIX}" ]                                                || \
    [ "${OUT_TFLITE_DEVICE_INSTALL_PREFIX}" == "/" ]                                            && \
        print-red "Root DIR for device install prefix is forbidden !!!"                         && \
        print-yellow "Reconfigure Device_install_prefix attribute in targets/.json file !"      && \
        return -6

    return 0
}

# Get CONTAINER_NAME from json
#   $1 - (mandatory) path to target config json
#   $2 - (mandatory) give container name as argument
function tflite-tools-host-get-container-name() {
    local PATH_TO_CONFIG_JSON=$1
    local -n CONTAINER_NAME_OUT=$2
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME_OUT TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS
}

# Build specified docker image as first argument
#   $1 - (mandatory) path to target config json
function tflite-tools-host-build-image() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    tflite-tools-git-log-wrapper
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-git-log-wrapper"
        return $rc
    }

    local TFLITE_TOOLS_TMP_FOLDER=${TF_LITE_TOOLS_DOCKER_FOLDER}/tmp

    rm -rf ${TFLITE_TOOLS_TMP_FOLDER}
    mkdir -p ${TFLITE_TOOLS_TMP_FOLDER}

    [ "${SDK_FULL_PATH}" == "null/null" ] || {
        ln ${SDK_FULL_PATH} ${TFLITE_TOOLS_TMP_FOLDER}/sdk.sh 2>/dev/null                                           || \
            rsync -a ${SDK_FULL_PATH} ${TFLITE_TOOLS_TMP_FOLDER}/sdk.sh                                             || \
            {
                echo "rsync -a ${SDK_FULL_PATH} ${TFLITE_TOOLS_TMP_FOLDER}/sdk.sh"
                print-red "Cannot add sdk sh file to tmp folder !!!"
                rm -rf ${TFLITE_TOOLS_TMP_FOLDER}
                return -2
            }
    }

    local GROUP=$(getent group $(id -g ${USER}) | cut -d ':' -f 1)

    local TFLITE_ARG_BASE_DIR=/mnt/tflite

    local TFLITE_NUM_CPU
    tflite-tools-get-number-of-threads TFLITE_NUM_CPU

    DOCKER_BUILDKIT=1 docker build                                                                                     \
            --build-arg TFLITE_ARG_HOST_USER_ID=$(id -u ${USER})                                                       \
            --build-arg TFLITE_ARG_HOST_GROUP_ID=$(id -g ${USER})                                                      \
            --build-arg TFLITE_ARG_HOST_USER=${USER}                                                                   \
            --build-arg TFLITE_ARG_HOST_GROUP=${GROUP}                                                                 \
            --build-arg TFLITE_ARG_BASE_DIR=${TFLITE_ARG_BASE_DIR}                                                     \
            --build-arg TFLITE_ARG_OS=${DEVICE_OS}                                                                     \
            --build-arg TFLITE_ARG_VERSION=${TFLITE_VERSION}                                                           \
            --build-arg TFLITE_ARG_RSYNC_DST=${TFLITE_RSYNC_DST}                                                       \
            --build-arg TFLITE_ARG_SDK_ENV_SETUP_SCRIPT=${SDK_ENV_SETUP_SCRIPT}                                        \
            --build-arg TFLITE_ARG_HEXAGON=${HEXAGON_DELEGATE}                                                         \
            --build-arg TFLITE_ARG_GPU=${GPU_DELEGATE}                                                                 \
            --build-arg TFLITE_ARG_XNNPACK=${XNNPACK_DELEGATE}                                                         \
            --build-arg TFLITE_ARG_NUM_CPU=${TFLITE_NUM_CPU}                                                         \
            --build-arg TFLITE_ARG_DEVICE_INSTALL_PREFIX=${TFLITE_DEVICE_INSTALL_PREFIX}                              \
            --build-arg TFLITE_ARG_TARGET_SYS=${TFLITE_TARGET_SYS}                              \
            --progress=plain --target ${IMAGE} ${TF_LITE_TOOLS_DOCKER_FOLDER} -t ${CONTAINER_NAME}                  || \
        {
            print-red "Build image failed !!!"
            rm -rf ${TFLITE_TOOLS_TMP_FOLDER}
            return -3
        }

    rm -rf ${TFLITE_TOOLS_TMP_FOLDER}

    print-green "Build image completed successfully !!!"
}

# Docker run container based on compiled docker image
#   $1 - (mandatory) path to target config json
function tflite-tools-host-run-container() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    local GROUP=$(getent group $(id -g ${USER}) | cut -d ':' -f 1)

    docker run                                                                                                         \
        -v /dev/bus/usb:/dev/bus/usb:ro                                                                                \
        -it -d --privileged -h tflite-tools-${CONTAINER_NAME} --user ${USER}                                           \
        --name ${CONTAINER_NAME} ${CONTAINER_NAME}                                                                  || \
        {
            print-red "Docker run failed !!!"
            return -2
        }

    # Propagate ssh and gitconfig to container
    docker exec --user ${USER} ${CONTAINER_NAME} mkdir /home/${USER}/.ssh                                           || \
        {
            print-red "docker mkdir ~/.ssh failed !!!"
            return -3
        }

    if [ -d ~/.ssh/ ]; then
        local f
        for f in ~/.ssh/*; do
            local BASE_NAME=`basename $f`
            test "${f}" = ~/.ssh/known_hosts && continue
            docker cp ${f} ${CONTAINER_NAME}:/home/${USER}/.ssh/${BASE_NAME}                                        || \
                {
                    print-red "Propagating .ssh/ to docker failed !!!"
                    return -4
                }
        done
        docker exec --user root ${CONTAINER_NAME} chown -R ${USER}:${GROUP} /home/${USER}/.ssh                      || \
            {
                print-red "Propagating .ssh/ to docker failed !!!"
                return -5
            }
    fi

    if [ -f ~/.gitconfig ]; then
        docker cp ~/.gitconfig ${CONTAINER_NAME}:/home/${USER}/.gitconfig                                           && \
            docker exec --user root ${CONTAINER_NAME} chown -R ${USER}:${GROUP} /home/${USER}/.gitconfig            || \
                {
                    print-red "Propagating .gitconfig to docker failed !!!"
                    return -6
                }
    fi

    if [ -f /etc/gitconfig ]; then
        docker cp /etc/gitconfig ${CONTAINER_NAME}:/etc/gitconfig                                                   || \
            {
                print-red "Propagating .gitconfig to docker failed !!!"
                return -7
            }
    fi

    print-green "Docker run successful !!!"
    print-green "docker attach to ${CONTAINER_NAME} !!!"
}

# Remove specified docker container as first argument
#   $1 - (mandatory) path to target config json
function tflite-tools-host-rm-container() {
    local PATH_TO_CONFIG_JSON=$1
    local CONTAINER_NAME

    tflite-tools-host-get-container-name ${PATH_TO_CONFIG_JSON} CONTAINER_NAME

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    docker rm ${CONTAINER_NAME}                                                                                     || \
        {
            print-red "Remove container failed !!!"
            return -2
        }

    print-green "Container removed successfully !!!"
}

# Start specified docker container as first argument
#   $1 - (mandatory) path to target config json
function tflite-tools-host-start-container() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    docker start ${CONTAINER_NAME}                                                                                  || \
        {
            print-red "Start container failed !!!"
            return -2
        }

    print-green "Container started successfully !!!"
}

# Stop specified docker container as first argument
#   $1 - (mandatory) path to target config json
function tflite-tools-host-stop-container() {
    local PATH_TO_CONFIG_JSON=$1
    local CONTAINER_NAME

    tflite-tools-host-get-container-name ${PATH_TO_CONFIG_JSON} CONTAINER_NAME

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    docker stop ${CONTAINER_NAME}                                                                                   || \
        {
            print-red "Stop container failed !!!"
            return -2
        }

    print-green "Container stopped successfully !!!"
}

# Docker host images clean up
function tflite-tools-host-images-cleanup() {
    docker rmi $(docker images | grep "^<none>" | awk '{print $3}')
    docker builder prune -a -f

    print-green "Host cleanup complete !!!"
}

# Deploy and sync target artifacts with LE device
#   $1 - (mandatory) deploy folder
function tflite-tools-host-deploy-le() {
    if [ $# -eq 0 ]; then
        echo "Error: At least one input argument is required to specify which folder to deploy to the device"
        return -1
    fi

    local DEPLOY_FOLDER=$1

    local CHECK_TIME
    [ -f ${DEPLOY_FOLDER}/sync_timestamp ] && CHECK_TIME="-anewer ${DEPLOY_FOLDER}/sync_timestamp"

    pushd ${DEPLOY_FOLDER}/usr 1>/dev/null                                                                          && \
        local TO_PUSH=`find . ${CHECK_TIME}`                                                                        && \
        [ ${#TO_PUSH} -ne 0 ]                                                                                       && \
        tar -cf ${DEPLOY_FOLDER}/sync.tar ${TO_PUSH}                                                                && \
        adb push ${DEPLOY_FOLDER}/sync.tar /data/                                                                   && \
        adb shell "tar --warning=no-timestamp -xf /data/sync.tar -C /usr/"                                          && \
        touch ${DEPLOY_FOLDER}/sync_timestamp

    adb shell "rm -f /data/sync.tar"
    popd 1>/dev/null
    rm -f ${DEPLOY_FOLDER}/sync.tar
    echo "Device update ready!"
}

# Deploy and sync target artifacts with LA device
#   $1 - (mandatory) deploy folder
function tflite-tools-host-deploy-la() {
    if [ $# -eq 0 ]; then
        echo "Error: At least one input argument is required to specify which folder to deploy to the device"
        return -1
    fi

    local DEPLOY_FOLDER=$1
    local ANDROID_PRODUCT_OUT=$DEPLOY_FOLDER
    adb sync vendor
    echo "Device update ready !"
}

# Deploy builder-dev target artifacts and sync with device
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-builder-dev() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    local TEMP_DIR=`mktemp -dt tmp-XXXXXX`
    print-blue "Selected deploy dir: ${TEMP_DIR}"

    local DIR=${TEMP_DIR}/${CONTAINER_NAME}

    [ -d ${DIR} ] || mkdir -p ${DIR}

    docker cp ${CONTAINER_NAME}:/mnt/tflite/deploy ${DIR}                                                           || \
        {
            print-red "Deploy builder-dev failed: docker cp failed !!!"
            rm -rf ${TEMP_DIR}
            return -2
        }

    tflite-tools-host-deploy-${DEVICE_OS} ${DIR}/deploy
    rc=$?

    rm -rf ${TEMP_DIR}
    return ${rc}
}

# Deploy builder-dev-dbg target artifacts and sync with device
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-builder-dev-dbg() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && print-red "parsing json failed !!!" && return -1

    local TEMP_DIR=`mktemp -dt tmp-XXXXXX`
    print-blue "Selected deploy dir: ${TEMP_DIR}"

    local DIR=${TEMP_DIR}/${CONTAINER_NAME}-dbg

    [ -d ${DIR} ] || mkdir -p ${DIR}

    docker cp ${CONTAINER_NAME}:/mnt/tflite/deploy_dbg ${DIR}                                                       || \
        {
            print-red "Deploy builder-dev-dbg failed: docker cp failed !!!"
            rm -rf ${TEMP_DIR}
            return -2
        }

    tflite-tools-host-deploy-${DEVICE_OS} ${DIR}/deploy_dbg
    local rc=$?

    rm -rf ${TEMP_DIR}
    return ${rc}
}

# Get release ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-rel-package() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    local PACKAGE_TYPE=""

    [ "${DEVICE_OS}" ==  "le" ] && {
        PACKAGE_TYPE="ipk"
    }

    [ "${DEVICE_OS}" == "lu" ] && {
        PACKAGE_TYPE="deb"
    }

    local ARTIFACTS_TEMP_DIR=$(mktemp -d)

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "source /mnt/tflite/scripts/all_scripts.sh
        tflite-tools-${PACKAGE_TYPE}-rel-pkg"

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/tflite_${TFLITE_VERSION}.${PACKAGE_TYPE} ${ARTIFACTS_TEMP_DIR} || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -1
        }

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/local_md5.log ${ARTIFACTS_TEMP_DIR}                          || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -2
        }

    rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}                                                           || \
        {
            print-red "FAILED: rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}"
            return -3
        }

    rm -r ${ARTIFACTS_TEMP_DIR}

    print-green "Host get release ${PACKAGE_TYPE} package successful !!!"

    return 0
}

# Get dev ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dev-package() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    local PACKAGE_TYPE=""

    [ "${DEVICE_OS}" ==  "le" ] && {
        PACKAGE_TYPE="ipk"
    }

    [ "${DEVICE_OS}" == "lu" ] && {
        PACKAGE_TYPE="deb"
    }

    local ARTIFACTS_TEMP_DIR=$(mktemp -d)

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "source /mnt/tflite/scripts/all_scripts.sh
        tflite-tools-dev-pkg"

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/tflite-dev_${TFLITE_VERSION}.${PACKAGE_TYPE} ${ARTIFACTS_TEMP_DIR} || \
        {
            print-red "Host get dev ${PACKAGE_TYPE} package failed !!!"
            return -1
        }

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/local_md5.log ${ARTIFACTS_TEMP_DIR}                          || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -2
        }

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/local_md5.log ${ARTIFACTS_TEMP_DIR}                          || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -3
        }

    rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}                                                           || \
        {
            print-red "FAILED: rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}"
            return -3
        }

    rm -r ${ARTIFACTS_TEMP_DIR}

    print-green "Host get dev ${PACKAGE_TYPE} package successful !!!"

    return 0
}

# Get dev package as tar.gz
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dev-tar-package() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    local ARTIFACTS_TEMP_DIR=$(mktemp -d)

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "source /mnt/tflite/scripts/all_scripts.sh
        tflite-tools-dev-pkg"

    docker cp ${CONTAINER_NAME}:/mnt/tflite/deploy_dev/tflite-dev-${TFLITE_VERSION}.tar.gz ${ARTIFACTS_TEMP_DIR}    || \
        {
            print-red "Host get dev package failed !!!"
            return -1
        }

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/local_md5.log ${ARTIFACTS_TEMP_DIR}                          || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -2
        }

    rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}                                                           || \
        {
            print-red "FAILED: rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}"
            return -3
        }

    rm -r ${ARTIFACTS_TEMP_DIR}

    print-green "Host get dev package successful !!!"

    return 0
}

# Get debug ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dbg-package() {
    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    local rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    local PACKAGE_TYPE=""

    [ "${DEVICE_OS}" ==  "le" ] && {
        PACKAGE_TYPE="ipk"
    }

    [ "${DEVICE_OS}" == "lu" ] && {
        PACKAGE_TYPE="deb"
    }

    local ARTIFACTS_TEMP_DIR=$(mktemp -d)

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "source /mnt/tflite/scripts/all_scripts.sh
        tflite-tools-${PACKAGE_TYPE}-dbg-pkg"

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/tflite-dbg_${TFLITE_VERSION}.${PACKAGE_TYPE} ${ARTIFACTS_TEMP_DIR} || \
        {
            print-red "Host get debug ${PACKAGE_TYPE} package failed !!!"
            return -1
        }

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/local_md5.log ${ARTIFACTS_TEMP_DIR}                          || \
        {
            print-red "Host get release ${PACKAGE_TYPE} package failed !!!"
            return -2
        }

    rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}                                                           || \
        {
            print-red "FAILED: rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}"
            return -3
        }

    rm -r ${ARTIFACTS_TEMP_DIR}

    print-green "Host get debug ${PACKAGE_TYPE} package successful !!!"

    return 0
}

# Deploy release ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-rel-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} rel
}

# Deploy dev ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-dev-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} dev
}

# Deploy debug ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-dbg-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} dbg
}

# Wrapper function to build, run, get ipk/deb pkg and zip
#   $1 - (mandatory) path to target config json
#   $2 - (mandatory) variant of ipk/deb
function tflite-tools-host-get-packages-archive() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    local IMAGE
    local DEVICE_OS
    local ADDITIONAL_TAG
    local TFLITE_VERSION
    local CONTAINER_NAME
    local TFLITE_RSYNC_DST
    local SDK_FULL_PATH
    local SDK_ENV_SETUP_SCRIPT
    local HEXAGON_DELEGATE
    local GPU_DELEGATE
    local XNNPACK_DELEGATE
    local TFLITE_DEVICE_INSTALL_PREFIX
    local TFLITE_TARGET_SYS

    tflite-tools-host-parse-json $PATH_TO_CONFIG_JSON IMAGE DEVICE_OS ADDITIONAL_TAG TFLITE_VERSION CONTAINER_NAME TFLITE_RSYNC_DST SDK_FULL_PATH SDK_ENV_SETUP_SCRIPT HEXAGON_DELEGATE GPU_DELEGATE XNNPACK_DELEGATE TFLITE_DEVICE_INSTALL_PREFIX TFLITE_TARGET_SYS

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    local VARIANT=$2

    [ ! "${VARIANT}" == "rel" ] && [ ! "${VARIANT}" == "dbg" ] && [ ! "${VARIANT}" == "dev" ]                       && \
        print-red "Variant input argument dbg or rel is required !!!"                                               && \
        return -1

    tflite-tools-host-build-image ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-build-image ${PATH_TO_CONFIG_JSON}"
        return $rc
    }

    tflite-tools-host-run-container ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-run-container ${PATH_TO_CONFIG_JSON}"
        return $rc
    }

    local PACKAGE_TYPE=""

    [ "${DEVICE_OS}" ==  "le" ] && {
        PACKAGE_TYPE="ipk"
    }

    [ "${DEVICE_OS}" == "lu" ] && {
        PACKAGE_TYPE="deb"
    }

    local PKG_SUFFIX=""
    local FUNCTION_VARIANT=""

    [ "${VARIANT}" == "rel" ] && {
        PKG_SUFFIX=""
        FUNCTION_VARIANT="-${PACKAGE_TYPE}"
    }

    [ "${VARIANT}" == "dev" ] && {
        PKG_SUFFIX="-dev"
    }

    [ "${VARIANT}" == "dbg" ] && {
        PKG_SUFFIX="-dbg"
        FUNCTION_VARIANT="-${PACKAGE_TYPE}"
    }

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "source /mnt/tflite/scripts/all_scripts.sh
        tflite-tools${FUNCTION_VARIANT}-${VARIANT}-pkg"

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: docker exec tflite-tools${FUNCTION_VARIANT}-${VARIANT}-pkg"
        return $rc
    }

    docker cp ${TF_LITE_TOOLS_DOCKER_FOLDER}/scripts/local/ ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/.

    docker exec --user ${USER} ${CONTAINER_NAME} bash -c "
        pushd \${TFLITE_PKG_DIR} 1>/dev/null                                                                        && \
        echo "\${TFLITE_DEVICE_INSTALL_PREFIX}" >> \${TFLITE_PKG_DIR}/tflite-sdk-install-prefix.txt                 && \
        zip packages_${PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip                                                  \
            tflite${PKG_SUFFIX}_${TFLITE_VERSION}.${PACKAGE_TYPE}                                                      \
            local_md5.log                                                                                              \
            tflite-sdk-install-prefix.txt                                                                              \
            ./local/*                                                                                               && \
        popd 1>/dev/null                                                                                            && \
        rm -rf \${TFLITE_PKG_DIR}/local/
"

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: docker exec while zipping tflite${PKG_SUFFIX}_${TFLITE_VERSION}.${PACKAGE_TYPE}"
        return $rc
    }

    local ARTIFACTS_TEMP_DIR=$(mktemp -d)

    docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/packages_${PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip ${ARTIFACTS_TEMP_DIR} || \
        {
            print-red "FAILED: docker cp ${CONTAINER_NAME}:/mnt/tflite/tflite_pkg/packages_${PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip ${ARTIFACTS_TEMP_DIR}"
            return -2
        }

    tflite-tools-host-stop-container ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-stop-container ${PATH_TO_CONFIG_JSON}"
        return $rc
    }

    tflite-tools-host-rm-container ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-rm-container ${PATH_TO_CONFIG_JSON}"
        return $rc
    }

    rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}                                                           || \
        {
            print-red "FAILED: rsync -aP ${ARTIFACTS_TEMP_DIR}/* ${TFLITE_RSYNC_DST}"
            return -2
        }

    rm -r ${ARTIFACTS_TEMP_DIR}

    return 0
}

# Remove installed packages from the device
function tflite-tools-packages-remove() {
    local rc

    source ${TF_LITE_TOOLS_DOCKER_FOLDER}/scripts/image/device/device.sh

    tflite-tools-device-packages-remove
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-packages-remove"
        return $rc
    }

    return 0
}

# Get number of threads of local machine
#   $1 - /output/ (mandatory) number of threads
function tflite-tools-get-number-of-threads() {
    local -n NUMBER_OF_THREADS=$1

    NUMBER_OF_THREADS=$(cat /proc/cpuinfo | awk '/^processor/{print $3}' | tail -1)
}

# Saves the git logs of tflite-tools in a txt file and syncs to TFLITE_RSYNC_DST
function tflite-tools-git-log-wrapper() {
    [ -z "${TFLITE_RSYNC_DST}" ] && {
        print-red "Rsync destination should be configured properly in json file !!!"
        return -1
    }

    local rc

    local GIT_LOGS_TEMP_DIR=$(mktemp -d)

    git -C ${TF_LITE_TOOLS_DOCKER_FOLDER} log --oneline |& tee ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt 1>/dev/null

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: command: git log  for ${TF_LITE_TOOLS_DOCKER_FOLDER}"
        return $rc
    }

    rsync -aP ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt ${TFLITE_RSYNC_DST}/. 1> /dev/null 2> /dev/null

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: commnad: rsync  from ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt to ${TFLITE_RSYNC_DST}/."
        return $rc
    }

    rm ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: command: rm ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt"
        return $rc
    }

    return 0
}

TF_LITE_TOOLS_DOCKER_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. && pwd )"

echo    "=================================="
echo -e "Docker environment setup ready !!!\n"

print-green "tflite-tools-host-build-image                                          <targets/.json>"
echo "    Build host image"
print-green "tflite-tools-host-run-container                                        <targets/.json>"
echo "    Run host container"
print-green "tflite-tools-host-rm-container                                         <targets/.json>"
echo "    Remove host container"
print-green "tflite-tools-host-start-container                                      <targets/.json>"
echo "    Start host container"
print-green "tflite-tools-host-stop-container                                       <targets/.json>"
echo "    Stop host container"
print-red "tflite-tools-host-images-cleanup"
echo "    Docker host images clean up"
print-blue "tflite-tools-host-deploy-builder-dev                                    <targets/.json>"
echo "    Deploy builder-dev target artifacts and sync with device"
print-blue "tflite-tools-host-deploy-builder-dev-dbg                                <targets/.json>"
echo "    Deploy builder-dev-dbg target artifacts and sync with device"
print-blue "tflite-tools-host-get-rel-package                                       <targets/.json>"
echo "    Get release ipk/deb package"
print-blue "tflite-tools-host-get-dev-package                                       <targets/.json>"
echo "    Get dev ipk/deb package"
print-blue "tflite-tools-host-get-dev-tar-package                                   <targets/.json>"
echo "    Get dev package as tar.gz"
print-blue "tflite-tools-host-get-dbg-package                                       <targets/.json>"
echo "    Get debug ipk/deb package"
print-blue "tflite-tools-host-deploy-rel-packages-archive                           <targets/.json>"
echo "    Deploy release ipk/deb package to host dir as zip"
print-blue "tflite-tools-host-deploy-dev-packages-archive                           <targets/.json>"
echo "    Deploy dev ipk/deb package to host dir as zip"
print-blue "tflite-tools-host-deploy-dbg-packages-archive                           <targets/.json>"
echo "    Deploy debug ipk/deb package to host dir as zip"
print-blue "tflite-tools-packages-remove"
echo "    Remove installed packages from the device"
