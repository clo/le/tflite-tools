#!/bin/bash

# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Parse json configuraiton
#   $1 - (mandatory) path to target config json
function tflite-tools-host-parse-json() {
    PATH_TO_CONFIG_JSON=$1

    [ ! -f "${PATH_TO_CONFIG_JSON}" ]                                                           && \
        print-red "Path to config json must be provided as first argument !!!"                  && \
        return -1

    local BUFFER=`cat ${PATH_TO_CONFIG_JSON}`

    TFLITE_OS=`echo ${BUFFER} |  jq '.Device_OS' | tr -d '"'`
    [ ! "${TFLITE_OS}" == "le" ] && [ ! "${TFLITE_OS}" == "la" ] && [ ! "${TFLITE_OS}" == "lu" ]&& \
        print-red "Reconfigure Device_OS attribute with correct data in targets/.json file !!!" && \
        print-yellow "Avaliable OS are le, lu and la."                                          && \
        return -2

    TFLITE_VERSION=`echo ${BUFFER} | jq '.TFLite_Version' | tr -d '"'`
    [ ! "${TFLITE_VERSION}" == "2.15.0" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.14.1" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.13.1" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.12.1" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.11.1" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.10.1" ]                                                       && \
    [ ! "${TFLITE_VERSION}" == "2.8.0" ]                                                        && \
    [ ! "${TFLITE_VERSION}" == "2.6.0" ]                                                        && \
        print-red "Reconfigure Version attribute with correct data in targets/.json file !!!"   && \
        print-yellow "Avaliable versions are:"                                                  && \
        print-yellow "2.15.0, 2.14.1, 2.13.1, 2.12.1, 2.11.1, 2.10.1, 2.8.0 and 2.6.0 !!!"      && \
        return -3

    SDK_PATH=`echo ${BUFFER} | jq '.SDK_path' | tr -d '"'`
    SDK_NAME=`echo ${BUFFER} | jq '.SDK_shell_file' | tr -d '"'`
    OUT_SDK_FULL_PATH=${SDK_PATH}/${SDK_NAME}

    [ "${TFLITE_OS}" == "le" ] || [ "${TFLITE_OS}" == "lu" ] && [ ! -f ${OUT_SDK_FULL_PATH} ]   && \
        print-red "SDK path and file are not specified !!!"                                     && \
        return -4

    BASE_DIR_LOCATION=`echo ${BUFFER} | jq '.Base_Dir_Location' | tr -d '"'`

    [ -z "${BASE_DIR_LOCATION}" ]                                                               || \
    [ "${BASE_DIR_LOCATION}" == "<path-to-base-dir>" ]                                          || \
    [ "${BASE_DIR_LOCATION}" == null ]                                                          && \
        print-red "Reconfigure BASE_DIR_LOCATION attribute in targets/.json file !!!"           && \
        return -5

    TFLITE_RSYNC_DST=`echo ${BUFFER} | jq '.TFLite_rsync_destination' | tr -d '"'`

    local DELEGATE_BUFFER=`echo ${BUFFER} | jq '.Delegates'`

    TFLITE_ENABLE_HEXAGON=`echo ${DELEGATE_BUFFER} | jq '.Hexagon_delegate' | tr -d '"'`
    TFLITE_ENABLE_GPU=`echo ${DELEGATE_BUFFER} | jq '.Gpu_delegate' | tr -d '"'`
    TFLITE_ENABLE_XNNPACK=`echo ${DELEGATE_BUFFER} | jq '.Xnnpack_delegate' | tr -d '"'`

    TFLITE_DEVICE_INSTALL_PREFIX=`echo ${BUFFER} | jq '.Device_install_prefix' | tr -d '"'`

    TFLITE_DEVICE_INSTALL_PREFIX=`echo ${TFLITE_DEVICE_INSTALL_PREFIX} | tr -s '/'`

    [ -z "${TFLITE_DEVICE_INSTALL_PREFIX}" ]                                                    || \
    [ "${TFLITE_DEVICE_INSTALL_PREFIX}" == "/" ]                                                && \
        print-red "Root DIR for device install prefix is forbidden !!!"                         && \
        print-yellow "Reconfigure Device_install_prefix attribute in targets/.json file !"      && \
        return -6

    export TFLITE_OS
    export TFLITE_VERSION
    export SDK_PATH
    export SDK_NAME
    export BASE_DIR_LOCATION
    export TFLITE_RSYNC_DST
    export TFLITE_ENABLE_HEXAGON
    export TFLITE_ENABLE_GPU
    export TFLITE_ENABLE_XNNPACK
    export TFLITE_DEVICE_INSTALL_PREFIX

    return 0
}

# Build TF Lite in host machine
#   $1 - (mandatory) path to target config json
function tflite-tools-setup() {
    local rc

    local PATH_TO_CONFIG_JSON=$1

    trap tflite-tools-export-env-variables RETURN

    tflite-tools-check-required-packages
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-check-required-packages"
        return $rc
    }

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    tflite-tools-git-log-wrapper
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-git-log-wrapper"
        return $rc
    }

    tflite-tools-common
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-common"
        return $rc
    }

    tflite-tools-common-${TFLITE_OS}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-common-${TFLITE_OS}"
        return $rc
    }

    tflite-tools-builder
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-builder"
        return $rc
    }

    return 0
}

# Remove TF Lite tools from host machine
#   $1 - (mandatory) path to target config json
function tflite-tools-remove() {
    local PATH_TO_CONFIG_JSON=$1
    local rc

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    rm -rf $BASE_DIR_LOCATION

    print-green "TF Lite tools removed"
    return 0
}

# Prepare directories and CMake
function tflite-tools-common() {
    local rc

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    tflite-tools-create-dirs
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-create-dirs"
        popd 1>/dev/null
        return $rc
    }

    pushd ${TFLITE_DOWNLOAD_DIR} 1>/dev/null

    tflite-tools-install-cmake
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-cmake"
        popd 1>/dev/null
        return $rc
    }

    popd 1>/dev/null

    return 0
}

# Install SDK, Proto buffer and JPEG Library for LE
function tflite-tools-common-le() {
    local rc

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common-le.sh

    pushd ${TFLITE_DOWNLOAD_DIR} 1>/dev/null

    tflite-tools-install-sdk
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-sdk"
        popd 1>/dev/null
        return $rc
    }

    tflite-tools-install-protoc-for-le
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-protoc-for-le"
        popd 1>/dev/null
        return $rc
    }

    tflite-tools-install-jpeg-turbo-for-le
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-jpeg-turbo-for-le"
        popd 1>/dev/null
        return $rc
    }

    popd 1>/dev/null

    return 0
}

# Install SDK, Proto buffer and JPEG Library for LE
function tflite-tools-common-lu() {
    tflite-tools-common-le
}

# Install NDK, Proto buffer and JPEG Library for LA
function tflite-tools-common-la() {
    local rc

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common-la.sh

    pushd ${TFLITE_DOWNLOAD_DIR} 1>/dev/null

    tflite-tools-install-ndk
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-ndk"
        popd 1>/dev/null
        return $rc
    }

    tflite-tools-install-protoc-for-la
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-protoc-for-la"
        popd 1>/dev/null
        return $rc
    }

    tflite-tools-install-jpeg-turbo-for-la
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-install-jpeg-turbo-for-la"
        popd 1>/dev/null
        return $rc
    }

    popd 1>/dev/null

    return 0
}

# Clone, apply patches and build the TF Lite
function tflite-tools-builder() {
    local rc

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-builder.sh
    source ${TFLITE_TOOLS_DIR}/scripts/image/tf-lite/tf-lite.sh

    tflite-tools-clone-tensorflow
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-clone-tensorflow"
        return $rc
    }

    tflite-tools-apply-patches
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-apply-patches"
        return $rc
    }

    tflite-tools-build-tensorflow
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-build-tensorflow"
        return $rc
    }

    return 0
}

# Get release ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-rel-package() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    # to get the necessary information of tflite directories
    tflite-tools-create-dirs
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-create-dirs"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/image/platform/platform.sh
    source ${TFLITE_TOOLS_DIR}/scripts/image/tf-lite/tf-lite.sh

    export TFLITE_PACKAGE_TYPE=""

    [ "${TFLITE_OS}" ==  "le" ] && {
        TFLITE_PACKAGE_TYPE="ipk"
    }

    [ "${TFLITE_OS}" == "lu" ] && {
        TFLITE_PACKAGE_TYPE="deb"
    }

    tflite-tools-${TFLITE_PACKAGE_TYPE}-rel-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-${TFLITE_PACKAGE_TYPE}-rel-pkg"
        return $rc
    }

    tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-rel-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-rel-pkg"
        return $rc
    }

    print-green "Host get release ${TFLITE_PACKAGE_TYPE} package successful !!!"

    return 0
}

# Get dev ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dev-package() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    # to get the necessary information of tflite directories
    tflite-tools-create-dirs
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-create-dirs"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/image/platform/platform.sh
    source ${TFLITE_TOOLS_DIR}/scripts/image/tf-lite/tf-lite.sh

    export TFLITE_PACKAGE_TYPE=""

    [ "${TFLITE_OS}" ==  "le" ] && {
        TFLITE_PACKAGE_TYPE="ipk"
    }

    [ "${TFLITE_OS}" == "lu" ] && {
        TFLITE_PACKAGE_TYPE="deb"
    }

    tflite-tools-dev-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-dev-pkg"
        return $rc
    }

    tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-dev-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-dev-pkg"
        return $rc
    }

    print-green "Host get dev ${TFLITE_PACKAGE_TYPE} package successful !!!"

    return 0
}

# Get dev package as tar.gz
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dev-tar-package() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    # to get the necessary information of tflite directories
    tflite-tools-create-dirs
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-create-dirs"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/image/platform/platform.sh
    source ${TFLITE_TOOLS_DIR}/scripts/image/tf-lite/tf-lite.sh

    tflite-tools-dev-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-dev-pkg"
        return $rc
    }

    tflite-tools-remote-pkg-sync ${TFLITE_DEPLOY_DEV_DIR}/tflite-dev-${TFLITE_VERSION}.tar.gz
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-pkg-sync ${TFLITE_DEPLOY_DEV_DIR}/tflite-dev-${TFLITE_VERSION}.tar.gz"
        return $rc
    }

    print-green "Host get dev package successful !!!"

    return 0
}

# Get debug ipk/deb package
#   $1 - (mandatory) path to target config json
function tflite-tools-host-get-dbg-package() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "parsing json failed !!!"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

    # to get the necessary information of tflite directories
    tflite-tools-create-dirs
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-create-dirs"
        return $rc
    }

    source ${TFLITE_TOOLS_DIR}/scripts/image/platform/platform.sh
    source ${TFLITE_TOOLS_DIR}/scripts/image/tf-lite/tf-lite.sh

    export TFLITE_PACKAGE_TYPE=""

    [ "${TFLITE_OS}" ==  "le" ] && {
        TFLITE_PACKAGE_TYPE="ipk"
    }

    [ "${TFLITE_OS}" == "lu" ] && {
        TFLITE_PACKAGE_TYPE="deb"
    }

    tflite-tools-${TFLITE_PACKAGE_TYPE}-dbg-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-${TFLITE_PACKAGE_TYPE}-dbg-pkg"
        return $rc
    }

    tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-dbg-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-sync-${TFLITE_PACKAGE_TYPE}-dbg-pkg"
        return $rc
    }

    print-green "Host get debug ${TFLITE_PACKAGE_TYPE} package successful !!!"

    return 0
}

# Deploy release ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-rel-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} rel
}

# Deploy dev ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-dev-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} dev
}

# Deploy debug ipk/deb package to host dir as zip
#   $1 - (mandatory) path to target config json
function tflite-tools-host-deploy-dbg-packages-archive() {
    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-get-packages-archive ${PATH_TO_CONFIG_JSON} dbg
}

# Wrapper function to build, run, get ipk/deb pkg and zip
#   $1 - (mandatory) path to target config json
#   $2 - (mandatory) variant of ipk/deb
function tflite-tools-host-get-packages-archive() {
    local rc

    local PATH_TO_CONFIG_JSON=$1
    local VARIANT=$2

    [ ! "${VARIANT}" == "rel" ] && [ ! "${VARIANT}" == "dbg" ] && [ ! "${VARIANT}" == "dev" ]   && \
        print-red "Variant input argument rel, dbg or dev is required !!!"                      && \
        return -1

    tflite-tools-setup ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-setup ${PATH_TO_CONFIG_JSON}"
        return $rc
    }

    export TFLITE_PACKAGE_TYPE=""

    [ "${TFLITE_OS}" ==  "le" ] && {
        TFLITE_PACKAGE_TYPE="ipk"
    }

    [ "${TFLITE_OS}" == "lu" ] && {
        TFLITE_PACKAGE_TYPE="deb"
    }

    source ${TFLITE_TOOLS_DIR}/scripts/image/platform/platform.sh

    local PKG_SUFFIX=""
    local FUNCTION_VARIANT=""

    [ "${VARIANT}" == "rel" ] && {
        PKG_SUFFIX=""
        FUNCTION_VARIANT="-${TFLITE_PACKAGE_TYPE}"
    }

    [ "${VARIANT}" == "dev" ] && {
        PKG_SUFFIX="-dev"
    }

    [ "${VARIANT}" == "dbg" ] && {
        PKG_SUFFIX="-dbg"
        FUNCTION_VARIANT="-${TFLITE_PACKAGE_TYPE}"
    }

    tflite-tools${FUNCTION_VARIANT}-${VARIANT}-pkg
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools${FUNCTION_VARIANT}-${VARIANT}-pkg"
        return $rc
    }

    cp -r ${TFLITE_TOOLS_DIR}/scripts/local/ ${TFLITE_PKG_DIR}

    pushd ${TFLITE_PKG_DIR} 1>/dev/null                                                         && \
    zip packages_${TFLITE_PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip                           \
        tflite${PKG_SUFFIX}_${TFLITE_VERSION}.${TFLITE_PACKAGE_TYPE}                               \
        local_md5.log                                                                              \
        ./local/*                                                                               && \
    popd 1>/dev/null                                                                            && \
    rm -rf ${TFLITE_PKG_DIR}/local/

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: Creating archive of .${TFLITE_PACKAGE_TYPE} files"
        popd 1>/dev/null
        return $rc
    }

    tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/packages_${TFLITE_PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-remote-pkg-sync ${TFLITE_PKG_DIR}/packages_${TFLITE_PACKAGE_TYPE}_${TFLITE_VERSION}_${VARIANT}.zip"
        return $rc
    }

    return 0
}

# Remove installed packages from the device
function tflite-tools-packages-remove() {
    local rc

    source ${TFLITE_TOOLS_DIR}/scripts/image/device/device.sh

    tflite-tools-device-packages-remove
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-device-packages-remove"
        return $rc
    }

    return 0
}

# Check for necessary packages
function tflite-tools-check-required-packages() {
    local NOT_INSTALLED_PKGS=""
    local REQUIRED_PKGS="curl wget git xz-utils zip unzip build-essential libncurses5 python3 sudo rsync   \
        file procps rsync file procps gcc-aarch64-linux-gnu g++-aarch64-linux-gnu"

    local UBUNTU_VERSION=$(lsb_release -rs)
    if [[ "$UBUNTU_VERSION" == "22.04" ]]; then
        REQUIRED_PKGS+= " adb fastboot"
    else
        REQUIRED_PKGS+= " android-tools-adb android-tools-fastboot"
    fi

    for PKG in ${REQUIRED_PKGS[@]}; do
        dpkg-query -s ${PKG} > /dev/null 2>&1 || NOT_INSTALLED_PKGS+=${PKG}" "
    done

    [ -n "${NOT_INSTALLED_PKGS}" ] && {
        print-red "THESE PACKAGES NEED TO BE INSTALLED:"
        echo ${NOT_INSTALLED_PKGS}
        print-blue "sudo apt install ${NOT_INSTALLED_PKGS}"
        return -1
    } || print-green "All the system required packages are installed!"

    return 0
}

# This function runs automatically after tflite-tools-setup completes successfully
# Creates a shell file with all environment variables and moves to current tflite scripts directory
# Main objective is to be able to source all_scripts.sh in tflite source directory
function tflite-tools-export-env-variables() {
    local ENV_VARS_FILE="${TFLITE_TOOLS_DIR}/scripts/image/env_variables.sh"

    touch ${ENV_VARS_FILE}

    echo "export TFLITE_OS=\"${TFLITE_OS}\""                                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_VERSION=\"${TFLITE_VERSION}\""                           >> ${ENV_VARS_FILE}
    echo "export SDK_PATH=\"${SDK_PATH}\""                                       >> ${ENV_VARS_FILE}
    echo "export SDK_NAME=\"${SDK_NAME}\""                                       >> ${ENV_VARS_FILE}
    echo "export BASE_DIR_LOCATION=\"${BASE_DIR_LOCATION}\""                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_ENABLE_HEXAGON=\"${TFLITE_ENABLE_HEXAGON}\""             >> ${ENV_VARS_FILE}
    echo "export TFLITE_ENABLE_GPU=\"${TFLITE_ENABLE_GPU}\""                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_ENABLE_XNNPACK=\"${TFLITE_ENABLE_XNNPACK}\""             >> ${ENV_VARS_FILE}

    echo "export SDK_TO_BUILD_TFLITE=\"${SDK_TO_BUILD_TFLITE}\""                 >> ${ENV_VARS_FILE}
    echo "export TFLITE_BASE_DIR=\"${TFLITE_BASE_DIR}\""                         >> ${ENV_VARS_FILE}
    echo "export TFLITE_DEPLOY_DIR=\"${TFLITE_DEPLOY_DIR}\""                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_DEPLOY_DBG_DIR=\"${TFLITE_DEPLOY_DBG_DIR}\""             >> ${ENV_VARS_FILE}
    echo "export TFLITE_DEPLOY_DEV_DIR=\"${TFLITE_DEPLOY_DEV_DIR}\""             >> ${ENV_VARS_FILE}
    echo "export TFLITE_PKG_DIR=\"${TFLITE_PKG_DIR}\""                           >> ${ENV_VARS_FILE}
    echo "export TFLITE_DOWNLOAD_DIR=\"${TFLITE_DOWNLOAD_DIR}\""                 >> ${ENV_VARS_FILE}
    echo "export TFLITE_STATUS=\"${TFLITE_STATUS}\""                             >> ${ENV_VARS_FILE}
    echo "export TFLITE_SRC_DIR=\"${TFLITE_SRC_DIR}\""                           >> ${ENV_VARS_FILE}
    echo "export TFLITE_HOST_TOOLS=\"${TFLITE_HOST_TOOLS}\""                     >> ${ENV_VARS_FILE}

    echo "export TFLITE_SDK_SETUP=\"${TFLITE_SDK_SETUP}\""                       >> ${ENV_VARS_FILE}
    echo "export TFLITE_TARGET_SYS=\"${TFLITE_TARGET_SYS}\""                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_SDK_BASE_DIR=\"${TFLITE_SDK_BASE_DIR}\""                 >> ${ENV_VARS_FILE}

    echo "export NDK_VERSION=\"${NDK_VERSION}\""                                 >> ${ENV_VARS_FILE}
    echo "export TFLITE_DEVICE_SYSROOT=\"${TFLITE_DEVICE_SYSROOT}\""             >> ${ENV_VARS_FILE}
    echo "export TFLITE_NDK_DIR=\"${TFLITE_NDK_DIR}\""                           >> ${ENV_VARS_FILE}

    echo "export TFLITE_ROOT_BUILD_DIR=\"${TFLITE_ROOT_BUILD_DIR}\""             >> ${ENV_VARS_FILE}
    echo "export TFLITE_TF_SRC_DIR=\"${TFLITE_TF_SRC_DIR}\""                     >> ${ENV_VARS_FILE}
    echo "export TFLITE_SRC_DIR=\"${TFLITE_SRC_DIR}\""                           >> ${ENV_VARS_FILE}
    echo "export TFLITE_DEPLOY_DEVICE_DIR=\"${TFLITE_DEPLOY_DEVICE_DIR}\""       >> ${ENV_VARS_FILE}
    echo "export TFLITE_BUILD_DIR=\"${TFLITE_BUILD_DIR}\""                       >> ${ENV_VARS_FILE}

    echo "export TFLITE_DEVICE_INSTALL_PREFIX=\"${TFLITE_DEVICE_INSTALL_PREFIX}\"" >> ${ENV_VARS_FILE}

echo "[[ \""'${PATH}'"\" == *\""'${TFLITE_HOST_TOOLS}'/usr/local/bin"\"* ]] || \
PATH="'${TFLITE_HOST_TOOLS}'"/usr/local/bin/:"'${PATH}'" "                       >> ${ENV_VARS_FILE}

    mv ${ENV_VARS_FILE} ${TFLITE_BASE_DIR}/scripts/.
}

# Saves the git logs of tflite-tools in a txt file and syncs to TFLITE_RSYNC_DST
function tflite-tools-git-log-wrapper() {
    [ -z "${TFLITE_RSYNC_DST}" ] && {
        print-red "Rsync destination should be configured properly in json file !!!"
        return -1
    }

    local rc

    local GIT_LOGS_TEMP_DIR=$(mktemp -d)

    git -C ${TFLITE_TOOLS_DIR} log --oneline |& tee ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt 1>/dev/null

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: command: git log --oneline for ${TFLITE_TOOLS_DIR}"
        return $rc
    }

    rsync -aP ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt ${TFLITE_RSYNC_DST}/. 1> /dev/null 2> /dev/null

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: commnad: rsync  from ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt to ${TFLITE_RSYNC_DST}/."
        return $rc
    }

    rm ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt

    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: command: rm ${GIT_LOGS_TEMP_DIR}/tflite-tools-git-logs.txt"
        return $rc
    }

    return 0
}

TFLITE_TOOLS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../ && pwd )"
source ${TFLITE_TOOLS_DIR}/scripts/image/common/tools.sh
source ${TFLITE_TOOLS_DIR}/scripts/image/remote/remote.sh

print-green "tflite-tools-setup                                             <path/to/targets/.json>"
echo "    Build TF Lite in host machine"
print-green "tflite-tools-remove                                            <path/to/targets/.json>"
echo "    Remove TF Lite tools from host machine"
print-blue "tflite-tools-host-get-rel-package                               <path/to/targets/.json>"
echo "    Get release ipk/deb package"
print-blue "tflite-tools-host-get-dev-package                               <path/to/targets/.json>"
echo "    Get dev ipk/deb package"
print-blue "tflite-tools-host-get-dev-tar-package                           <path/to/targets/.json>"
echo "    Get dev package as tar.gz"
print-blue "tflite-tools-host-get-dbg-package                               <path/to/targets/.json>"
echo "    Get debug ipk/deb package"
print-blue "tflite-tools-host-deploy-rel-packages-archive                   <path/to/targets/.json>"
echo "    Deploy release ipk/deb package to host dir as zip"
print-blue "tflite-tools-host-deploy-dev-packages-archive                   <path/to/targets/.json>"
echo "    Deploy dev ipk/deb package to host dir as zip"
print-blue "tflite-tools-host-deploy-dbg-packages-archive                   <path/to/targets/.json>"
echo "    Deploy debug ipk/deb package to host dir as zip"
print-blue "tflite-tools-packages-remove"
echo "    Remove installed ipk packages from the device"
