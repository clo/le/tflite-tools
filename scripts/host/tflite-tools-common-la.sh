#!/bin/bash

# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Install NDK
function tflite-tools-install-ndk() {
    local rc

    export NDK_VERSION="r25"

    [ ! -d ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE} ] && {
        rm -rf ${TFLITE_NDK_DIR}/*

        echo "Installing ${SDK_TO_BUILD_TFLITE}..."

        wget --quiet https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux.zip                  && \
            unzip -q android-ndk-${NDK_VERSION}-linux.zip                                                           && \
            mv android-ndk-${NDK_VERSION} ${TFLITE_NDK_DIR}                                                         && \
            rm -rf android-ndk-*

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}
            rm -rf android-ndk-*
            return $rc
        }

        mkdir -p ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}
    } || echo "${SDK_TO_BUILD_TFLITE}: Already installed"

    TFLITE_NDK_DIR=${TFLITE_NDK_DIR}/android-ndk-${NDK_VERSION}
    export TFLITE_DEVICE_SYSROOT=${TFLITE_NDK_DIR}/toolchains/llvm/prebuilt/linux-x86_64/sysroot
    mkdir -p ${TFLITE_DEPLOY_DBG_DIR}/vendor/lib64/

    return 0
}

# Install Proto Buffer for LA
function tflite-tools-install-protoc-for-la() {
    local rc

    local PROTO_BUFF_VERSION="3.17.3"

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION} ] && {
        echo "Installing Proto Buffer for LA..."

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt

        local TF_LITE_NUM_CPU
        tflite-tools-get-number-of-threads TF_LITE_NUM_CPU

        wget --quiet https://github.com/protocolbuffers/protobuf/releases/download/v${PROTO_BUFF_VERSION}/protobuf-cpp-${PROTO_BUFF_VERSION}.tar.gz && \
            tar xf protobuf-cpp-${PROTO_BUFF_VERSION}.tar.gz                                                        && \
            cd protobuf-${PROTO_BUFF_VERSION}                                                                       && \
            mkdir -p x86_64_build                                                                                   && \
            cd x86_64_build/                                                                                        && \
            ../configure --disable-shared CC="${TFLITE_NDK_DIR}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang" CXX="${TFLITE_NDK_DIR}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang++" --prefix=${PWD}/x86_64_pb_install CXXFLAGS="-fPIC" && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            cp -v x86_64_pb_install/bin/protoc ${TFLITE_HOST_TOOLS}/usr/bin/                                           \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cp -v x86_64_pb_install/lib/libprotobuf.a ${TFLITE_HOST_TOOLS}/usr/lib/                                    \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cp -v -r x86_64_pb_install/include/google ${TFLITE_HOST_TOOLS}/usr/include                                 \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cd ../                                                                                                  && \
            mkdir -p arm64_build                                                                                    && \
            cd arm64_build/                                                                                         && \
            ../configure --disable-shared --prefix=${PWD}/arm64_pb_install CC="${TFLITE_NDK_DIR}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang --sysroot=${TFLITE_DEVICE_SYSROOT}" CXX="${TFLITE_NDK_DIR}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang++ --sysroot=${TFLITE_DEVICE_SYSROOT}" CFLAGS="-target aarch64-none-linux-android21 -llog" --host=aarch64-linux --with-protoc=../x86_64_build/x86_64_pb_install/bin/protoc ARCH=arm64 CXXFLAGS="-fPIC -target aarch64-none-linux-android21 -llog" CROSS_COMPILE=aarch64-linux-gnu && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            cp -v arm64_pb_install/lib/libprotobuf.a ${TFLITE_DEVICE_SYSROOT}/usr/lib/aarch64-linux-android/           \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cp -v -r arm64_pb_install/include/ ${TFLITE_DEVICE_SYSROOT}/include                                        \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt             && \
            cd ../../                                                                                               && \
            rm -rf protobuf*

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt
            rm -rf protobuf*
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/protobuffer_${PROTO_BUFF_VERSION}
    } || echo "Proto Buffer ${PROTO_BUFF_VERSION} for LA: Already installed"

    return 0
}

# Install JPEG Library for LA
function tflite-tools-install-jpeg-turbo-for-la() {
    local rc

    local JPEG_VERSION="2.0.90"

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library ] && {
        echo "Installing JPEG Library for LA..."

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt

        local TF_LITE_NUM_CPU
        tflite-tools-get-number-of-threads TF_LITE_NUM_CPU

        wget --quiet --output-document libjpeg-${JPEG_VERSION}.tar.xz https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/${JPEG_VERSION}.tar.gz && \
            tar xf libjpeg-${JPEG_VERSION}.tar.xz                                                                   && \
            cd libjpeg-turbo-${JPEG_VERSION}/                                                                       && \
            cmake -DCMAKE_TOOLCHAIN_FILE=${TFLITE_NDK_DIR}/build/cmake/android.toolchain.cmake                         \
                -DANDROID_ABI=arm64-v8a                                                                                \
                -DCMAKE_INSTALL_DEFAULT_PREFIX=${PWD}/install_dir                                                      \
                -DCMAKE_POSITION_INDEPENDENT_CODE=ON                                                                   \
                -DWITH_TURBOJPEG=FALSE                                                                                 \
                -DENABLE_SHARED=FALSE .                                                                             && \
            make -j${TF_LITE_NUM_CPU}                                                                               && \
            make -j${TF_LITE_NUM_CPU} install                                                                       && \
            cp -v ./install_dir/lib64/libjpeg.a* ${TFLITE_DEVICE_SYSROOT}/usr/lib/aarch64-linux-android/               \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt                  && \
            cp -v ./install_dir/include/* ${TFLITE_DEVICE_SYSROOT}/include/                                            \
                >> ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt                  && \
            cd ../                                                                                                  && \
            rm -rf libjpeg*

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library_backup.txt
            rm -rf libjpeg*
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/jpeg_${JPEG_VERSION}_library
    } || echo "JPEG Library ${JPEG_VERSION} for LA: Already installed"

    return 0
}

# Uninstall NDK and also JPEG Turbo and Proto Buffer, because there is dependency
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-ndk() {
    local rc

    local NDK_VERSION="r25"

    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local NDK_TO_REMOVE=${BASE_DIR}/${COMMON_DIR}/ndk
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_NDK="tflite-NDK"

    [ -d ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK} ]                                                                && \
    [ -d ${NDK_TO_REMOVE} ] && {

        pushd ${NDK_TO_REMOVE} 1>/dev/null

        tflite-tools-uninstall-protoc-for-la
        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-uninstall-protoc-for-la"
            popd 1>/dev/null
            return $rc
        }

        tflite-tools-uninstall-jpeg-turbo-for-la
        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-uninstall-jpeg-turbo-for-la"
            popd 1>/dev/null
            return $rc
        }

        rm -rf android-ndk-${NDK_VERSION}
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}

        popd 1>/dev/null
    } || echo "${TFLITE_BUILDED_BY_NDK}: Already removed"

    return 0
}

# Uninstall Proto Buffer for LA
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-protoc-for-la() {
    local rc

    local PROTO_BUFF_VERSION="3.17.3"

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_NDK="tflite-NDK"

    [ -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/protobuffer_${PROTO_BUFF_VERSION} ] && {
        [ ! -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt ] && {
            print-red "Proto Buffer ${PROTO_BUFF_VERSION} for LA: Can not remove, try to install Proto buffer"
            return -1
        }

        local REMOVE_INFO=`cat ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt | rev | cut -d "'" -f 2 | rev`

        echo "${REMOVE_INFO}" | tr '\n' ' ' | xargs rm -rf

        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/protobuffer_${PROTO_BUFF_VERSION}
        rm -rf  ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/protobuffer_${PROTO_BUFF_VERSION}_backup.txt
    } || echo "Proto Buffer ${PROTO_BUFF_VERSION} for LA: Already removed"

    return 0
}

# Uninstall JPEG Turbo for LA
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-jpeg-turbo-for-la() {
    local rc

    local JPEG_VERSION="2.0.90"

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_BUILDED_BY_NDK="tflite-NDK"

    [ -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/jpeg_${JPEG_VERSION}_library ] && {
        [ ! -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/jpeg_${JPEG_VERSION}_library_backup.txt ] && {
            print-red "JPEG Library ${JPEG_VERSION} for LA: Can not remove, try to install JPEG Library"
            return -1
        }

        local REMOVE_INFO=`cat ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/jpeg_${JPEG_VERSION}_library_backup.txt | rev | cut -d "'" -f 2 | rev`

        echo "${REMOVE_INFO}" | tr '\n' ' ' | xargs rm -rf

        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/jpeg_${JPEG_VERSION}_library
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_NDK}/jpeg_${JPEG_VERSION}_library_backup.txt
    } || echo "JPEG Library ${JPEG_VERSION} for LA: Already removed"

    return 0
}
