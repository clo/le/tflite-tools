#!/bin/bash

# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Create essential directories
function tflite-tools-create-dirs() {
    local rc

    export SDK_TO_BUILD_TFLITE=""

    [ ${TFLITE_OS} == "la" ] && SDK_TO_BUILD_TFLITE="tflite-NDK"

    [ ${TFLITE_OS} == "le" ] || [ ${TFLITE_OS} == "lu" ] && {
        tflite-tools-which-sdk ${SDK_NAME} SDK_TO_BUILD_TFLITE

        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK"
            return $rc
        }
    }

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"

    export TFLITE_BASE_DIR="${BASE_DIR}/${SDK_TO_BUILD_TFLITE}/tflite-${TFLITE_VERSION}-${TFLITE_OS}"
    export TFLITE_DEPLOY_DIR="${TFLITE_BASE_DIR}/deploy"
    export TFLITE_DEPLOY_DBG_DIR=${TFLITE_BASE_DIR}/deploy_dbg
    export TFLITE_DEPLOY_DEV_DIR=${TFLITE_BASE_DIR}/deploy_dev
    export TFLITE_PKG_DIR=${TFLITE_BASE_DIR}/tflite_pkg
    export TFLITE_DOWNLOAD_DIR=${BASE_DIR}/${COMMON_DIR}/download
    export TFLITE_NDK_DIR=${BASE_DIR}/${COMMON_DIR}/ndk
    export TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status
    export TFLITE_SDK_BASE_DIR=${BASE_DIR}/${COMMON_DIR}/sdk
    export TFLITE_SRC_DIR=${TFLITE_BASE_DIR}/src

    mkdir -p ${TFLITE_BASE_DIR}
    mkdir -p ${TFLITE_DEPLOY_DIR}
    mkdir -p ${TFLITE_DEPLOY_DBG_DIR}
    mkdir -p ${TFLITE_DEPLOY_DEV_DIR}
    mkdir -p ${TFLITE_PKG_DIR}
    mkdir -p ${TFLITE_DOWNLOAD_DIR}
    mkdir -p ${TFLITE_NDK_DIR}
    mkdir -p ${TFLITE_STATUS}
    mkdir -p ${TFLITE_SDK_BASE_DIR}
    mkdir -p ${TFLITE_SRC_DIR}

    mkdir -p ${TFLITE_BASE_DIR}/scripts/
    mkdir -p ${TFLITE_DOWNLOAD_DIR}/tf-lite-${TFLITE_VERSION}/

    cp -r ${TFLITE_TOOLS_DIR}/scripts/image/* ${TFLITE_BASE_DIR}/scripts/.

    cp -r ${TFLITE_TOOLS_DIR}/patches/* ${TFLITE_DOWNLOAD_DIR}/.

    export TFLITE_HOST_TOOLS=${BASE_DIR}/${COMMON_DIR}/host-tools

    mkdir -p ${TFLITE_HOST_TOOLS}
    mkdir -p ${TFLITE_HOST_TOOLS}/usr/local/bin/
    mkdir -p ${TFLITE_HOST_TOOLS}/usr/local/share/
    mkdir -p ${TFLITE_HOST_TOOLS}/usr/bin/
    mkdir -p ${TFLITE_HOST_TOOLS}/usr/lib/
    mkdir -p ${TFLITE_HOST_TOOLS}/usr/include/

    mkdir -p ${TFLITE_DEPLOY_DBG_DIR}/usr/lib

    return 0
}

# Install CMake
function tflite-tools-install-cmake() {
    local rc

    local CMAKE_VERSION="3.20.2"

    [ ! -f ${TFLITE_STATUS}/cmake-${CMAKE_VERSION} ] && {
        echo "Installing CMake ${CMAKE_VERSION}..."

        touch ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt

        wget --quiet https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz && \
            tar -zxf cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz                                    \
                >> ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt                           && \
            cp -v -r cmake-${CMAKE_VERSION}-linux-x86_64/bin/* ${TFLITE_HOST_TOOLS}/usr/local/bin/ \
                >> ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt                           && \
            cp -v -r cmake-${CMAKE_VERSION}-linux-x86_64/share/* ${TFLITE_HOST_TOOLS}/usr/local/share/ \
                >> ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt                           && \
            rm -rf cmake*

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}
            rm -rf ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt
            rm -rf cmake*
            return $rc
        }

        touch ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}

    } || echo "CMake ${CMAKE_VERSION}: Already installed"

    [[ "${PATH}" == *"${TFLITE_HOST_TOOLS}/usr/local/bin"* ]] || PATH=${TFLITE_HOST_TOOLS}/usr/local/bin/:${PATH}

    return 0
}

# Uninstall CMake
#   $1 - (mandatory) path to target config json
function tflite-tools-uninstall-cmake() {
    local rc

    local PATH_TO_CONFIG_JSON=$1

    local CMAKE_VERSION="3.20.2"

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"

    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    [ -f ${TFLITE_STATUS}/cmake-${CMAKE_VERSION} ] && {
        [ ! -f ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt ] && {
            print-red "CMake ${CMAKE_VERSION}: Can not remove, try to install CMake"
            return -1
        }

        local REMOVE_INFO=`cat ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt | rev | cut -d "'" -f 2 | rev`

        echo "${REMOVE_INFO}" | tr '\n' ' ' | xargs rm -rf

        rm -rf ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}
        rm -rf ${TFLITE_STATUS}/cmake-${CMAKE_VERSION}_backup.txt
    } || echo "CMake ${CMAKE_VERSION}: Already removed"

    return 0
}

# Check by which sdk has been builded the current tflite
#   $1 - /input/ (mandatory) sdk name
#   $2 - /output/ (mandatory) tflite builded by sdk
function tflite-tools-which-sdk() {
    local rc

    local SDK_NAME=$1
    local -n OUT_TFLITE_BUILDED_BY_SDK=$2

    local TOOLCHAIN_NAME=""
    local AU=""

    AU=${SDK_NAME##*toolchain-}
    AU=${AU%%[^0-9]*}

    TOOLCHAIN_NAME=${SDK_NAME%%toolchain*}
    TOOLCHAIN_NAME=$(echo ${TOOLCHAIN_NAME} | rev | cut -d '-' -f2 | rev)

    OUT_TFLITE_BUILDED_BY_SDK="tflite-${TOOLCHAIN_NAME}-toolchain-${AU}"

    return 0
}

# Get number of threads of local machine
#   $1 - /output/ (mandatory) number of threads
function tflite-tools-get-number-of-threads() {
    local -n NUMBER_OF_THREADS=$1

    NUMBER_OF_THREADS=$(cat /proc/cpuinfo | awk '/^processor/{print $3}' | tail -1)
}
