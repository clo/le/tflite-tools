#!/bin/bash

# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Clone the Tensorflow from codelinaro
function tflite-tools-clone-tensorflow() {
    local rc

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/tflite_source_${TFLITE_VERSION}_${TFLITE_OS} ] && {
        echo "Git Clone TF Lite source..."

        git clone --progress https://git.codelinaro.org/clo/lc/external/github.com/tensorflow/tensorflow.git --single-branch --branch v${TFLITE_VERSION} ${TFLITE_SRC_DIR}

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/tflite_source_${TFLITE_VERSION}_${TFLITE_OS}
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/tflite_source_${TFLITE_VERSION}_${TFLITE_OS}
    } || echo "TF Lite source: Already installed"

    export TFLITE_ROOT_BUILD_DIR=${TFLITE_BASE_DIR}/build
    export TFLITE_TF_SRC_DIR=${TFLITE_BASE_DIR}/src
    export TFLITE_SRC_DIR=${TFLITE_TF_SRC_DIR}/tensorflow/lite/c
    export TFLITE_DEPLOY_DEVICE_DIR=/tmp/mount-docker
    export TFLITE_BUILD_DIR=${TFLITE_ROOT_BUILD_DIR}/tflite

    return 0
}

# Apply patches for the current TF Lite version
function tflite-tools-apply-patches() {
    local rc

    local PATCHES_DIR=${TFLITE_TOOLS_DIR}/patches

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/patches_applied_for_${TFLITE_VERSION}_${TFLITE_OS} ] && {
        echo "Applying patches for TF Lite: ${TFLITE_VERSION}_${TFLITE_OS}..."

        git -C ${TFLITE_TF_SRC_DIR} am --reject --whitespace=fix ${PATCHES_DIR}/tf-lite-${TFLITE_VERSION}/0*.patch

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/patches_applied_for_${TFLITE_VERSION}_${TFLITE_OS}
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/patches_applied_for_${TFLITE_VERSION}_${TFLITE_OS}
    } || echo "Patches for TF Lite: ${TFLITE_VERSION}_${TFLITE_OS} Already applied"

    return 0
}

# Build the TF Lite
function tflite-tools-build-tensorflow() {
    local rc

    [ ! -f ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/build_tflite_${TFLITE_VERSION}_${TFLITE_OS} ] && {
        echo "Building TF Lite: ${TFLITE_VERSION}_${TFLITE_OS}..."

        tflite-tools-clean-build-install

        rc=$?
        [ $rc -ne 0 ] && {
            rm -rf ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/build_tflite_${TFLITE_VERSION}_${TFLITE_OS}
            return $rc
        }

        touch ${TFLITE_STATUS}/${SDK_TO_BUILD_TFLITE}/build_tflite_${TFLITE_VERSION}_${TFLITE_OS}
    } || echo "TF Lite: ${TFLITE_VERSION}_${TFLITE_OS} Already Builded"

    return 0
}

# Remove specific tensorflow version
#   $1 - (mandatory) path to target config json
function tflite-tools-remove-tensorflow() {
    local rc

    local PATH_TO_CONFIG_JSON=$1

    tflite-tools-host-parse-json ${PATH_TO_CONFIG_JSON}
    rc=$?
    [ $rc -ne 0 ] && {
        print-red "FAILED: tflite-tools-host-parse-json"
        return $rc
    }

    local TFLITE_BUILDED_BY_SDK=""

    [ ${TFLITE_OS} == "la" ] && TFLITE_BUILDED_BY_SDK="tflite-NDK"

    [ ${TFLITE_OS} == "le" ] || [ ${TFLITE_OS} == "lu" ] && {
        source ${TFLITE_TOOLS_DIR}/scripts/host/tflite-tools-common.sh

        tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK
        rc=$?
        [ $rc -ne 0 ] && {
            print-red "FAILED: tflite-tools-which-sdk ${SDK_NAME} TFLITE_BUILDED_BY_SDK"
            return $rc
        }
    }

    local BASE_DIR="${BASE_DIR_LOCATION}"
    local COMMON_DIR="common"
    local TFLITE_STATUS=${BASE_DIR}/${COMMON_DIR}/.status

    local TFLITE_TO_REMOVE="${BASE_DIR}/${TFLITE_BUILDED_BY_SDK}/tflite-${TFLITE_VERSION}-${TFLITE_OS}"

    [ -f ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/tflite_source_${TFLITE_VERSION}_${TFLITE_OS} ] && {
        rm -rf ${TFLITE_TO_REMOVE}

        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/build_tflite_${TFLITE_VERSION}_${TFLITE_OS}
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/patches_applied_for_${TFLITE_VERSION}_${TFLITE_OS}
        rm -rf ${TFLITE_STATUS}/${TFLITE_BUILDED_BY_SDK}/tflite_source_${TFLITE_VERSION}_${TFLITE_OS}

    } || echo "TF Lite: ${TFLITE_VERSION}_${TFLITE_OS} Already removed"

    return 0
}
