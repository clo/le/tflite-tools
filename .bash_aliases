#!/bin/bash

# Copyright (c) 2021-2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Some more ls aliases
alias ls='ls --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Source  all scripts
source ${TFLITE_BASE_DIR}/scripts/all_scripts.sh

# Update ssh config with current user and key exchange method if they are missing
[ -f ~/.ssh/config ]                                                                                     && \
    grep -zq "Host *.*user ${TFLITE_HOST_USER}.*KexAlgorithms +diffie-hellman-group1-sha1" ~/.ssh/config || \
    echo -e "Host *\n\tuser ${TFLITE_HOST_USER}\n\tKexAlgorithms +diffie-hellman-group1-sha1" >> ~/.ssh/config
