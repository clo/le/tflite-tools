# TF Lite Host

This project builds the Tensorflow Lite library for LE and LA devices which run **Linux Embedded** or **Linux Android** on **Arm64**.
The host build can generate :
1. An IPK package containing the TFLite apps and libraries, which can be installed on the devices
2. Deploy and deploy debug folders which can be synced with the device. This functionality is working for all **Linux Embedded** and **Linux Android** targets from host file system

## Table of Contents

* [Prerequisites](#Prerequisites)
  * [Ubuntu Version](#Ubuntu_Version)
  * [Ubuntu Packages](#Ubuntu_Packages)
  * [How to increase Max user watches and max user instances on host system](#Max_user_watches)
* [Helper Scripts And Configuration](#Helper_Scripts_And_Configuration)
  * [How to fill out Configuration JSON File](#How_to_fill_out_Configuration_JSON_File)
  * [Helper Scripts](#Helper_Scripts)
* [Local Sync And Uninstall Scripts](#Local_Sync_And_Uninstall_Scripts)
* [Development Workflow](#Development_Workflow)
  * [Initial One Time Setup](#Initial_One_Time_Setup)
  * [Continuous Development After Initial Setup](#Continuous_Development_After_Initial_Setup)
* [Examples For Development](#Examples_For_Development)
  * [Incremental Build](#Incremental_Build)
  * [Clean Build](#Clean_Build)
  * [TF Lite Applications](#TF_Lite_Applications)

<div id="Prerequisites">

## Prerequisites

<div id="Ubuntu_Version">

### Ubuntu Version

Ubuntu 18.04 or Ubuntu 20.04 is required

<div id="Ubuntu_Packages">

### Ubuntu Packages

jq must be installed on the host (one time)

```bash
sudo apt install -y jq
```

<div id="Max_user_watches">

### Max user watches and max user instances must be increased on the host system

**Add these two lines to */etc/sysctl.conf* and reboot the PC**

```bash
fs.inotify.max_user_instances=8192
fs.inotify.max_user_watches=542288
```

<div id="Helper_Scripts_And_Configuration">

## Helper Scripts And Configuration

<div id="How_to_fill_out_Configuration_JSON_File">

### How to fill out Configuration JSON File

The json file must contain certain data :

 1. ***MANDATORY*** - **Image** - Available options are "builder" and "builder-dev" - builder image is the release variant, while builder-dev is development variant
 2. ***MANDATORY*** - **Device_OS** - Available options are "la" and "le" - Linux Android or Linux Embedded
 3. ***OPTIONAL*** - **Additional_tag** - Additional tag to be appended to the name of the base folder - allows for personalization of the names of the base folder according to their purpose (to not set an additional tag just leave the value for this field empty)
 4. ***MANDATORY*** - **TFLite_Version** - Available options are "2.15.0", "2.14.1", "2.13.1", "2.12.1", "2.11.1", "2.10.1", "2.8.0" and "2.6.0" - tensor flow lite version to be used
 5. ***OPTIONAL*** - **TFLite_rsync_destination** - To be able to sync packages to this destination folder
 6. ***MANDATORY for LE*** - **SDK_path** - Path to the directory in the work environment that contains the SDK .sh and json file generated after eSDK compilation. Parameter is mandatory only for LE
 ***PATH MUST BE ABSOLUTE, DO NOT USE A RELATIVE PATH!***
 7. ***MANDATORY for LE*** - **SDK_shell_file** - Name of the shell file inside eSDK directory. Parameter is mandatory only for LE
 8. ***MANDATORY*** - **Base_Dir_Location** - Path to work base dir
 9. ***MANDATORY*** - **Device_install_prefix** - Device install prefix to choose where to be located libraries binaries and include files, default value is /opt/qti/tflite-sdk/

The json files must be created in the ```targets/``` directory. Example json files for each supported combination are located in ```targets/``` directory.

***Once you have created the configuration file in ```targets/```, the image can be built***

The functions in ```\/scripts/host/host_env_setup.sh``` provide the necessary setup commands. Some of them of them receive the path to the configuration json file as their first and only argument, as shown in the examples bellow

<div id="Helper_Scripts">

### Helper Scripts

***In order for the functions inside host_env_setup.sh to work on the host, the script must be sourced***

```bash
source scripts/host/host_env_setup.sh
```

- ```tflite-tools-setup ./targets/<config.json>``` - This function builds TF Lite in host machine
- ```tflite-tools-remove ./targets/<config.json>``` - This function removes TF Lite tools from host machine
- ```tflite-tools-host-get-rel-package ./targets/<config.json>``` - This function gets release ipk/deb package.
- ```tflite-tools-host-get-dev-package ./targets/<config.json>``` - This function gets dev ipk/deb package.
- ```tflite-tools-host-get-dev-tar-package ./targets/<config.json>``` - This function gets dev package as tar.gz.
- ```tflite-tools-host-get-dbg-package ./targets/<config.json>``` - This function gets debug ipk/deb package.
- ```tflite-tools-host-deploy-rel-packages-archive ./targets/<config.json>``` - This function deploys release ipk/deb package from container to host dir as zip.
- ```tflite-tools-host-deploy-dev-packages-archive ./targets/<config.json>``` - This function deploys dev ipk/deb package from container to host dir as zip.
- ```tflite-tools-host-deploy-dbg-packages-archive ./targets/<config.json>``` - This function deploys debug ipk/deb package from container to host dir as zip.
- ```tflite-tools-packages-remove``` - This function removes packages installed by tflite .ipk/.deb file.

- ```tflite-tools-device-prepare``` - This function prepares device
- ```tflite-tools-device-deploy``` - This function deploys Release variant to the device
- ```tflite-tools-device-deploy-dbg``` - This function deploys Debug variant with debug symbols to the device
- ```tflite-tools-device-packages-remove``` - This function removes installed packages from the device

- ```tflite-tools-clean-build-install``` - This function cleans, configures, builds and installs tf lite
- ```tflite-tools-incremental-build-install``` - This function incrementally builds and installs tf lite
- ```tflite-tools-build-<tflite-target>``` - This function builds desired tf lite target (benchmark-model/label-image/multimodel-label-image)
- ```tflite-tools-build-all``` - This function builds all tf lite targets
- ```tflite-tools-clean``` - This function performs environment cleanup
- ```tflite-tools-dev-pkg``` - This function creates ipk dev package with interface headers and libraries
- ```tflite-tools-ipk-dev-pkg-deploy``` - This function deploys ipk dev package to the device
- ```tflite-tools-deb-dev-pkg-deploy``` - This function deploys deb dev package to the device
- ```tflite-tools-ipk-dbg-pkg``` - This function creates package build artifacts as ipk debug package
- ```tflite-tools-deb-dbg-pkg``` - This function creates package build artifacts as deb debug package
- ```tflite-tools-ipk-dbg-pkg-deploy``` - This function deploys ipk debug package to the device
- ```tflite-tools-deb-dbg-pkg-deploy``` - This function deploys deb debug package to the device
- ```tflite-tools-ipk-rel-pkg``` - This function creates package build artifacts as ipk release package
- ```tflite-tools-deb-rel-pkg``` - This function creates package build artifacts as deb release package
- ```tflite-tools-ipk-rel-pkg-deploy``` - This function deploys ipk release package to the device
- ```tflite-tools-deb-rel-pkg-deploy``` - This function deploys deb release package to the device

- ```tflite-tools-remote-sync-ipk-rel-pkg``` - This function syncs release ipk packages with the remote target
- ```tflite-tools-remote-sync-deb-rel-pkg``` - This function syncs release deb packages with the remote target
- ```tflite-tools-remote-sync-ipk-dbg-pkg``` - This function syncs debug ipk packages with the remote target
- ```tflite-tools-remote-sync-deb-dbg-pkg``` - This function syncs debug deb packages with the remote target
- ```tflite-tools-remote-sync-ipk-dev-pkg``` - This function syncs dev ipk packages with the remote target
- ```tflite-tools-remote-sync-deb-dev-pkg``` - This function syncs dev deb packages with the remote target

<div id="Local_Sync_And_Uninstall_Scripts">

## Local Sync And Uninstall Scripts

If ipk files needs to be deployed to device connected to another pc, then ipk files can be copied to that pc with *tflite-tools-host-get-rel-package*, *tflite-tools-host-get-dbg-package* or *tflite-tools-host-get-dev-package* scripts. Depending whether Linux or Windows is used on the pc (where device is connected) can be used corresponding scripts to update ipk's to the device. Those scripts also needs to be copied to the pc (where device is connected). Installed packages can be uninstalled later using the corresponding uninstall script provided below.

***Please note that these scripts are deleting all successfully installed ipk files from the folder on pc (where device is connected)***

***Please note that device needs to be prepared (disable verify, root, remount, file system remount as rw) for update before invoking these scripts***

***Please note that adb needs to be available in the path variable for windows and linux***

Details can be found in
[Sync Documentation](scripts/local/Sync.md)

<div id="Development_Workflow">

## Development Workflow

<div id="Initial_One_Time_Setup">

### Initial One Time Setup

#### Password Protected SSH Key

***Please note that this step needs to be invoked only once after console is started***

If use of password protected ssh key is needed, ssh agent needs to be used:

```bash
ssh-agent -s > ~/work/.ssh_agent.info
. ~/work/.ssh_agent.info
ssh-add ~/.ssh/<private-ssh-key-name>
```

#### Prepare Device Connected To Local PC After Image Or Metabuild Flash

***Please note that this step MUST be invoked only once after device, connected to local PC, is flashed with new images or metabuild***

```bash
tflite-tools-device-prepare
adb disable-verity
adb reboot
```

#### Prepare Device Connected To Local PC

***Please note that this step needs to be invoked only once after device, connected to local PC, is started***

***Please note that this step needs to be invoked only once after console is started***

```bash
tflite-tools-device-prepare
```

<div id="Continuous_Development_After_Initial_Setup">

### Continuous Development After Initial Setup

#### Source Code Location

```bash
ls -la ${TFLITE_TF_SRC_DIR}
```

#### Do Your Modifications And Src Code Development

```bash
<editor> <filename>
```

#### Compiling Code

Incremental build and install tf lite

```bash
tflite-tools-incremental-build-install
```

Clean, configure, build and install tf lite

```bash
tflite-tools-clean-build-install
```

#### Syncing Build Artifacts To The Device

Device update with release variant

```bash
tflite-tools-device-deploy
```

Or debug variant

```bash
tflite-tools-device-deploy-dbg
```

#### Clean Compiled Code

```bash
tflite-tools-tf-lite-clean
```

### Generate Dev Package

```bash
tflite-tools-dev-pkg
```

### Generate Debug Package

```bash
tflite-tools-ipk-dbg-pkg
```

### Generate Release Package

```bash
tflite-tools-ipk-rel-pkg
```

<div id="Examples_For_Development">

## Examples For Development

<div id="Incremental_Build">

### Incremental Build

- Scenario is:
  - No ssh key password protection
  - Locally connected device
  - Device with disabled verity
  - Incremental Build
  - Sync release variant

#### Initial Setup

Prepare the environment

```bash
# Prepare Device For Update
tflite-tools-device-prepare
```

#### Continuous Development

Modify src code in

```bash
ls -la ${TFLITE_TF_SRC_DIR}
```

Build, package code and update the device with release variant

```bash
# Build code and update the device with release variant
tflite-tools-incremental-build-install && tflite-tools-device-deploy
```

<div id="Clean_Build">

### Clean Build

- Scenario is:
  - No ssh key password protection
  - Locally connected device
  - Freshly flashed device with metabuild
  - Clean Build
  - Sync debug variant

#### Initial Setup

Prepare the environment

```bash
# Prepare Device For Update After Meta Flashing
tflite-tools-device-prepare
adb disable-verity
adb reboot
# Prepare Device For Update
tflite-tools-device-prepare
```

#### Continuous Development

Modify src code in

```bash
ls -la ${TFLITE_TF_SRC_DIR}
```

Build code and update the device with debug variant

```bash
# Build, package code and update the device with release variant
tflite-tools-clean-build-install && tflite-tools-device-deploy-dbg
```

<div id="TF_Lite_Applications">

### TF Lite Applications

### label_image

#### Help:
```bash
INFO:
usage: label_image <flags>
Flags:
        --num_threads=-1                                int32   optional        number of threads used for inference on CPU.
        --max_delegated_partitions=0                    int32   optional        Max number of partitions to be delegated.
        --min_nodes_per_partition=0                     int32   optional        The minimal number of TFLite graph nodes of a partition that has to be reached for it to be delegated.A negative value or 0 means to use the default choice of each delegate.
        --first_delegate_node_index=0                   int32   optional        The index of the first node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is 0.
        --last_delegate_node_index=2147483647           int32   optional        The index of the last node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is INT_MAX.
        --delegate_serialize_dir=                       string  optional        Directory to be used by delegates for serializing any model data. This allows the delegate to save data into this directory to reduce init time after the first run. Currently supported by NNAPI delegate with specific backends on Android. Note that delegate_serialize_token is also required to enable this feature.
        --delegate_serialize_token=                     string  optional        Model-specific token acting as a namespace for delegate serialization. Unique tokens ensure that the delegate doesn't read inapplicable/invalid data. Note that delegate_serialize_dir is also required to enable this feature.
        --use_xnnpack=false                             bool    optional        explicitly apply the XNNPACK delegate. Note the XNNPACK delegate could be implicitly applied by the TF Lite runtime regardless the value of this parameter. To disable this implicit application, set the value to false explicitly.
        --use_gpu=false                                 bool    optional        use gpu
        --gpu_precision_loss_allowed=true               bool    optional        Allow to process computation in lower precision than FP32 in GPU. By default, it's enabled.
        --gpu_experimental_enable_quant=true            bool    optional        Whether to enable the GPU delegate to run quantized models or not. By default, it's enabled.
        --gpu_inference_for_sustained_speed=false       bool    optional        Whether to prefer maximizing the throughput. This mode will help when the same delegate will be used repeatedly on multiple inputs. This is supported on non-iOS platforms. By default, it's disabled.
        --gpu_backend=                                  string  optional        Force the GPU delegate to use a particular backend for execution, and fail if unsuccessful. Should be one of: cl, gl
        --use_hexagon=false                             bool    optional        Use Hexagon delegate
        --hexagon_lib_path=/data/local/tmp              string  optional        The library path for the underlying Hexagon libraries. The library path ONLY for the libhexagon_nn_skel*.so files. For libhexagon_interface.so, it needs to be on a system library search path such as LD_LIBRARY_PATH.
        --hexagon_profiling=false                       bool    optional        Enables Hexagon profiling
        --accelerated, -a: [0|1] use Android NNAPI or not
        --allow_fp16, -f: [0|1], allow running fp32 models with fp16 or not
        --count, -c: loop interpreter->Invoke() for certain times
        --gl_backend, -g: [0|1]: use GL GPU Delegate on Android
        --hexagon_delegate, -j: [0|1]: use Hexagon Delegate on Android
        --input_mean, -b: input mean
        --input_std, -s: input standard deviation
        --image, -i: image_name.bmp
        --labels, -l: labels for the model
        --tflite_model, -m: model_name.tflite
        --profiling, -p: [0|1], profiling or not
        --num_results, -r: number of results to show
        --threads, -t: number of threads
        --verbose, -v: [0|1] print more information
        --warmup_runs, -w: number of warmup runs
        --xnnpack_delegate, -x [0:1]: xnnpack delegate
        --help, -h: Print this help message
```

#### Example:
```bash
adb shell "label_image -l <path to labels in device>/labels.txt -i <path to image in device>/.bmp -m <path to Models in device>/.tflite -c 10 -j 1 -p 1"
```

### benchmark_model
#### Help:
```bash
INFO:
usage: benchmark_model <flags>
Flags:
        --num_runs=50                                   int32   optional        expected number of runs, see also min_secs, max_secs
        --min_secs=1                                    float   optional        minimum number of seconds to rerun for, potentially making the actual number of runs to be greater than num_runs
        --max_secs=150                                  float   optional        maximum number of seconds to rerun for, potentially making the actual number of runs to be less than num_runs. Note if --max-secs is exceeded in the middle of a run, the benchmark will continue to the end of the run but will not start the next run.
        --run_delay=-1                                  float   optional        delay between runs in seconds
        --run_frequency=-1                              float   optional        Execute at a fixed frequency, instead of a fixed delay.Note if the targeted rate per second cannot be reached, the benchmark would start the next run immediately, trying its best to catch up. If set, this will override run_delay.
        --num_threads=-1                                int32   optional        number of threads
        --use_caching=false                             bool    optional        Enable caching of prepacked weights matrices in matrix multiplication routines. Currently implies the use of the Ruy library.
        --benchmark_name=                               string  optional        benchmark name
        --output_prefix=                                string  optional        benchmark output prefix
        --warmup_runs=1                                 int32   optional        minimum number of runs performed on initialization, to allow performance characteristics to settle, see also warmup_min_secs
        --warmup_min_secs=0.5                           float   optional        minimum number of seconds to rerun for, potentially making the actual number of warm-up runs to be greater than warmup_runs
        --verbose=false                                 bool    optional        Whether to log parameters whose values are not set. By default, only log those parameters that are set by parsing their values from the commandline flags.
        --dry_run=false                                 bool    optional        Whether to run the tool just with simply loading the model, allocating tensors etc. but without actually invoking any op kernels.
        --report_peak_memory_footprint=false            bool    optional        Report the peak memory footprint by periodically checking the memory footprint. Internally, a separate thread will be spawned for this periodic check. Therefore, the performance benchmark result could be affected.
        --memory_footprint_check_interval_ms=50         int32   optional        The interval in millisecond between two consecutive memory footprint checks. This is only used when --report_peak_memory_footprint is set to true.
        --graph=                                        string  optional        graph file name
        --input_layer=                                  string  optional        input layer names
        --input_layer_shape=                            string  optional        input layer shape
        --input_layer_value_range=                      string  optional        A map-like string representing value range for *integer* input layers. Each item is separated by ':', and the item value consists of input layer name and integer-only range values (both low and high are inclusive) separated by ',', e.g. input1,1,2:input2,0,254
        --input_layer_value_files=                      string  optional        A map-like string representing value file. Each item is separated by ',', and the item value consists of input layer name and value file path separated by ':', e.g. input1:file_path1,input2:file_path2. In case the input layer name contains ':' e.g. "input:0", escape it with "\:". If the input_name appears both in input_layer_value_range and input_layer_value_files, input_layer_value_range of the input_name will be ignored. The file format is binary and it should be array format or null separated strings format.
        --allow_fp16=false                              bool    optional        allow fp16
        --require_full_delegation=false                 bool    optional        require delegate to run the entire graph
        --enable_op_profiling=true                      bool    optional        enable op profiling
        --max_profiling_buffer_entries=1024             int32   optional        max initial profiling buffer entries
        --allow_dynamic_profiling_buffer_increase=false bool    optional        allow dynamic increase on profiling buffer entries
        --profiling_output_csv_file=                    string  optional        File path to export profile data as CSV, if not set prints to stdout.
        --print_preinvoke_state=false                   bool    optional        print out the interpreter internals just before calling Invoke. The internals will include allocated memory size of each tensor etc.
        --print_postinvoke_state=false                  bool    optional        print out the interpreter internals just before benchmark completes (i.e. after all repeated Invoke calls complete). The internals will include allocated memory size of each tensor etc.
        --release_dynamic_tensors=false                 bool    optional        Ensure dynamic tensor's memory is released when they are not used.
        --optimize_memory_for_large_tensors=0           int32   optional        Optimize memory usage for large tensors with sacrificing latency.
        --disable_delegate_clustering=false             bool    optional        Disable delegate clustering.
        --output_filepath=                              string  optional        File path to export outputs layer as binary data.
        --tensor_name_display_length=25                 int32   optional        The number of characters to show for the tensor's name when printing the interpeter's state, defaults to 25.
        --tensor_type_display_length=15                 int32   optional        The number of characters to show for the tensor's type when printing the interpeter's state, defaults to 15.
        --alloc_type_display_length=18                  int32   optional        The number of characters to show for the tensor's allocation type when printing the interpeter's state, defaults to 18.
        --help=false                                    bool    optional        Print out all supported flags if true.
        --num_threads=-1                                int32   optional        number of threads used for inference on CPU.
        --max_delegated_partitions=0                    int32   optional        Max number of partitions to be delegated.
        --min_nodes_per_partition=0                     int32   optional        The minimal number of TFLite graph nodes of a partition that has to be reached for it to be delegated.A negative value or 0 means to use the default choice of each delegate.
        --first_delegate_node_index=0                   int32   optional        The index of the first node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is 0.
        --last_delegate_node_index=2147483647           int32   optional        The index of the last node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is INT_MAX.
        --delegate_serialize_dir=                       string  optional        Directory to be used by delegates for serializing any model data. This allows the delegate to save data into this directory to reduce init time after the first run. Currently supported by NNAPI delegate with specific backends on Android. Note that delegate_serialize_token is also required to enable this feature.
        --delegate_serialize_token=                     string  optional        Model-specific token acting as a namespace for delegate serialization. Unique tokens ensure that the delegate doesn't read inapplicable/invalid data. Note that delegate_serialize_dir is also required to enable this feature.
        --use_xnnpack=false                             bool    optional        explicitly apply the XNNPACK delegate. Note the XNNPACK delegate could be implicitly applied by the TF Lite runtime regardless the value of this parameter. To disable this implicit application, set the value to false explicitly.
        --external_delegate_path=                       string  optional        The library path for the underlying external.
        --external_delegate_options=                    string  optional        A list of comma-separated options to be passed to the external delegate. Each option is a colon-separated key-value pair, e.g. option_name:option_value.
        --use_gpu=false                                 bool    optional        use gpu
        --gpu_precision_loss_allowed=true               bool    optional        Allow to process computation in lower precision than FP32 in GPU. By default, it's enabled.
        --gpu_experimental_enable_quant=true            bool    optional        Whether to enable the GPU delegate to run quantized models or not. By default, it's enabled.
        --gpu_inference_for_sustained_speed=false       bool    optional        Whether to prefer maximizing the throughput. This mode will help when the same delegate will be used repeatedly on multiple inputs. This is supported on non-iOS platforms. By default, it's disabled.
        --gpu_backend=                                  string  optional        Force the GPU delegate to use a particular backend for execution, and fail if unsuccessful. Should be one of: cl, gl
        --use_hexagon=false                             bool    optional        Use Hexagon delegate
        --hexagon_lib_path=/data/local/tmp              string  optional        The library path for the underlying Hexagon libraries. The library path ONLY for the libhexagon_nn_skel*.so files. For libhexagon_interface.so, it needs to be on a system library search path such as LD_LIBRARY_PATH.
        --hexagon_profiling=false                       bool    optional        Enables Hexagon profiling
```

#### Example:
```bash
adb shell "benchmark_model --graph=<path to Models in device>/.tflite --enable_op_profiling=false --use_gpu=true --num_runs=1 --warmup_runs=0 --max_secs=300"
```

### multimodel_label_image
#### Help:
```bash
--accelerated, -a: [0|1], use Android NNAPI or not
--old_accelerated, -d: [0|1], use old Android NNAPI delegate or not
--preferences, -X: [comma-separated] preferences for the choosen delegate in hex
--allow_fp16, -f: [0|1], allow running fp32 models with fp16 or not
--count, -c: loop interpreter->Invoke() for certain times
--gl_backend, -g: [0|1]: use GL GPU Delegate on Android
--hexagon_delegate, -j: [0|1]: use Hexagon Delegate on Android
--input_mean, -b: input mean
--input_std, -s: input standard deviation
--image, -i: image_name.bmp
--labels, -l: labels for the model
--tflite_model, -m: [comma-separated] tflite model(s)
--profiling, -p: [0|1], profiling or not
--num_results, -r: number of results to show
--threads, -t: number of threads
--verbose, -v: [0|1] print more information
--warmup_runs, -w: number of warmup runs
--frequency, -J: [comma-separated] desired frequency for model(s)
--accelerator_list, -A: [comma-separated] accelrator list where
         0: CPU only
         1: Android NNAPI
         2: Hexagon Delegate
         3: GPU Delegate
--parser, -n: Select parser type. 0 - classification, 1 - detection, 2 - segmentation
--dump_tensors, -u: [0|1] Dump output tensors
--threshold, -y: Threshold float value
--xnnpack_delegate, -x [0:1]: xnnpack delegate
```

#### Example:
```bash
adb shell "multimodel_label_image -l <path to labels in device>/labels.txt                         \
  -i <path to image in device>/.bmp                                                                \
  -m <path to Models in device>/.tflite,<path to Models in device>/.tflite,<path to Models in device>/.tflite \
  -c 20                                                                                            \
  -A 3,2,0                                                                                         \
  -p 1"
```
#### Note:
count of models(-m) should be equal to count of accelerators(-A)
models are listed with commas and there should be NO spaces

### object_detect_run_eval
#### Help:
```bash
INFO:
usage: object_detect_run_eval <flags>
Flags:
        --model_file=                                   string  optional        Path to test tflite model file.
        --model_output_labels=                          string  optional        Path to labels that correspond to output of model. E.g. in case of COCO-trained SSD model, this is the path to file where each line contains a class detected by the model in correct order, starting from background.
        --ground_truth_images_path=                     string  optional        Path to ground truth images. These will be evaluated in alphabetical order of filenames
        --ground_truth_proto=                           string  optional        Path to file containing tflite::evaluation::ObjectDetectionGroundTruth proto in binary serialized format. If left empty, mAP numbers are not output.
        --output_file_path=                             string  optional        File to output to. Contains only metrics proto if debug_mode is off, and per-image predictions also otherwise.
        --debug_mode=false                              bool    optional        Whether to enable debug mode. Per-image predictions are written to the output file along with metrics.
        --num_interpreter_threads=1                     int32   optional        Number of interpreter threads to use for inference.
        --delegate=                                     string  optional        Delegate to use for inference, if available. Must be one of {'nnapi', 'gpu', 'xnnpack', 'hexagon'}
        --help=false                                    bool    optional        Print out all supported flags if true.
        --num_threads=-1                                int32   optional        number of threads used for inference on CPU.
        --max_delegated_partitions=0                    int32   optional        Max number of partitions to be delegated.
        --min_nodes_per_partition=0                     int32   optional        The minimal number of TFLite graph nodes of a partition that has to be reached for it to be delegated.A negative value or 0 means to use the default choice of each delegate.
        --first_delegate_node_index=0                   int32   optional        The index of the first node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is 0.
        --last_delegate_node_index=2147483647           int32   optional        The index of the last node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is INT_MAX.
        --delegate_serialize_dir=                       string  optional        Directory to be used by delegates for serializing any model data. This allows the delegate to save data into this directory to reduce init time after the first run. Currently supported by NNAPI delegate with specific backends on Android. Note that delegate_serialize_token is also required to enable this feature.
        --delegate_serialize_token=                     string  optional        Model-specific token acting as a namespace for delegate serialization. Unique tokens ensure that the delegate doesn't read inapplicable/invalid data. Note that delegate_serialize_dir is also required to enable this feature.
        --use_xnnpack=false                             bool    optional        explicitly apply the XNNPACK delegate. Note the XNNPACK delegate could be implicitly applied by the TF Lite runtime regardless the value of this parameter. To disable this implicit application, set the value to false explicitly.
        --use_gpu=false                                 bool    optional        use gpu
        --gpu_precision_loss_allowed=true               bool    optional        Allow to process computation in lower precision than FP32 in GPU. By default, it's enabled.
        --gpu_experimental_enable_quant=true            bool    optional        Whether to enable the GPU delegate to run quantized models or not. By default, it's enabled.
        --gpu_inference_for_sustained_speed=false       bool    optional        Whether to prefer maximizing the throughput. This mode will help when the same delegate will be used repeatedly on multiple inputs. This is supported on non-iOS platforms. By default, it's disabled.
        --gpu_backend=                                  string  optional        Force the GPU delegate to use a particular backend for execution, and fail if unsuccessful. Should be one of: cl, gl
        --use_hexagon=false                             bool    optional        Use Hexagon delegate
        --hexagon_lib_path=/data/local/tmp              string  optional        The library path for the underlying Hexagon libraries. The library path ONLY for the libhexagon_nn_skel*.so files. For libhexagon_interface.so, it needs to be on a system library search path such as LD_LIBRARY_PATH.
        --hexagon_profiling=false                       bool    optional        Enables Hexagon profiling
```

#### Example:
```bash
adb shell "object_detect_run_eval --model_file=<path to Models in device>/.tflite --ground_truth_images_path=<path to ground truth images> --ground_truth_proto=<path to file containing tflite::evaluation::ObjectDetectionGroundTruth proto in binary serialized format in device>/.pb --model_output_labels=<path to labels that correspond to output of model in device>/.txt --delegate=gpu"
```

### image_classify_run_eval
#### Help:
```bash
INFO:
usage: image_classify_run_eval <flags>
Flags:
        --model_file=                                   string  optional        Path to test tflite model file.
        --model_output_labels=                          string  optional        Path to labels that correspond to output of model. E.g. in case of mobilenet, this is the path to label file where each label is in the same order as the output of the model.
        --ground_truth_images_path=                     string  optional        Path to ground truth images. These will be evaluated in alphabetical order of filename
        --ground_truth_labels=                          string  optional        Path to ground truth labels, corresponding to alphabetical ordering of ground truth images.
        --denylist_file_path=                           string  optional        Path to denylist file (optional) where each line is a single integer that is equal to index number of denylisted image.
        --output_file_path=                             string  optional        File to output metrics proto to.
        --num_images=0                                  int32   optional        Number of examples to evaluate, pass 0 for all examples. Default: 0
        --num_interpreter_threads=1                     int32   optional        Number of interpreter threads to use for inference.
        --delegate=                                     string  optional        Delegate to use for inference, if available. Must be one of {'nnapi', 'gpu', 'hexagon', 'xnnpack'}
        --help=false                                    bool    optional        Print out all supported flags if true.
        --num_threads=-1                                int32   optional        number of threads used for inference on CPU.
        --max_delegated_partitions=0                    int32   optional        Max number of partitions to be delegated.
        --min_nodes_per_partition=0                     int32   optional        The minimal number of TFLite graph nodes of a partition that has to be reached for it to be delegated.A negative value or 0 means to use the default choice of each delegate.
        --first_delegate_node_index=0                   int32   optional        The index of the first node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is 0.
        --last_delegate_node_index=2147483647           int32   optional        The index of the last node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is INT_MAX.
        --delegate_serialize_dir=                       string  optional        Directory to be used by delegates for serializing any model data. This allows the delegate to save data into this directory to reduce init time after the first run. Currently supported by NNAPI delegate with specific backends on Android. Note that delegate_serialize_token is also required to enable this feature.
        --delegate_serialize_token=                     string  optional        Model-specific token acting as a namespace for delegate serialization. Unique tokens ensure that the delegate doesn't read inapplicable/invalid data. Note that delegate_serialize_dir is also required to enable this feature.
        --use_xnnpack=false                             bool    optional        explicitly apply the XNNPACK delegate. Note the XNNPACK delegate could be implicitly applied by the TF Lite runtime regardless the value of this parameter. To disable this implicit application, set the value to false explicitly.
        --use_gpu=false                                 bool    optional        use gpu
        --gpu_precision_loss_allowed=true               bool    optional        Allow to process computation in lower precision than FP32 in GPU. By default, it's enabled.
        --gpu_experimental_enable_quant=true            bool    optional        Whether to enable the GPU delegate to run quantized models or not. By default, it's enabled.
        --gpu_inference_for_sustained_speed=false       bool    optional        Whether to prefer maximizing the throughput. This mode will help when the same delegate will be used repeatedly on multiple inputs. This is supported on non-iOS platforms. By default, it's disabled.
        --gpu_backend=                                  string  optional        Force the GPU delegate to use a particular backend for execution, and fail if unsuccessful. Should be one of: cl, gl
        --use_hexagon=false                             bool    optional        Use Hexagon delegate
        --hexagon_lib_path=/data/local/tmp              string  optional        The library path for the underlying Hexagon libraries. The library path ONLY for the libhexagon_nn_skel*.so files. For libhexagon_interface.so, it needs to be on a system library search path such as LD_LIBRARY_PATH.
        --hexagon_profiling=false                       bool    optional        Enables Hexagon profiling
```

#### Example:
```bash
adb shell "image_classify_run_eval --model_file=<path to Models in device>/.tflite --ground_truth_images_path=<path to ground truth images in device> --ground_truth_labels=<path to ground truth labels in device>/.txt --model_output_labels=<path to labels that correspond to output of model in device> --delegate=gpu"
```

### inf_diff_run_eval
#### Help:
```bash
INFO:
usage: inf_diff_run_eval <flags>
Flags:
        --model_file=                                   string  optional        Path to test tflite model file.
        --output_file_path=                             string  optional        File to output metrics proto to.
        --num_runs=50                                   int32   optional        Number of runs of test & reference inference each. Default value: 50
        --num_interpreter_threads=1                     int32   optional        Number of interpreter threads to use for test inference.
        --delegate=                                     string  optional        Delegate to use for test inference, if available. Must be one of {'nnapi', 'gpu', 'hexagon', 'xnnpack', 'coreml'}
        --help=false                                    bool    optional        Print out all supported flags if true.
        --num_threads=-1                                int32   optional        number of threads used for inference on CPU.
        --max_delegated_partitions=0                    int32   optional        Max number of partitions to be delegated.
        --min_nodes_per_partition=0                     int32   optional        The minimal number of TFLite graph nodes of a partition that has to be reached for it to be delegated.A negative value or 0 means to use the default choice of each delegate.
        --first_delegate_node_index=0                   int32   optional        The index of the first node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is 0.
        --last_delegate_node_index=2147483647           int32   optional        The index of the last node that could be delegated. Used only when TFLITE_DEBUG_DELEGATE is defined. Default is INT_MAX.
        --delegate_serialize_dir=                       string  optional        Directory to be used by delegates for serializing any model data. This allows the delegate to save data into this directory to reduce init time after the first run. Currently supported by NNAPI delegate with specific backends on Android. Note that delegate_serialize_token is also required to enable this feature.
        --delegate_serialize_token=                     string  optional        Model-specific token acting as a namespace for delegate serialization. Unique tokens ensure that the delegate doesn't read inapplicable/invalid data. Note that delegate_serialize_dir is also required to enable this feature.
        --use_xnnpack=false                             bool    optional        explicitly apply the XNNPACK delegate. Note the XNNPACK delegate could be implicitly applied by the TF Lite runtime regardless the value of this parameter. To disable this implicit application, set the value to false explicitly.
        --use_gpu=false                                 bool    optional        use gpu
        --gpu_precision_loss_allowed=true               bool    optional        Allow to process computation in lower precision than FP32 in GPU. By default, it's enabled.
        --gpu_experimental_enable_quant=true            bool    optional        Whether to enable the GPU delegate to run quantized models or not. By default, it's enabled.
        --gpu_inference_for_sustained_speed=false       bool    optional        Whether to prefer maximizing the throughput. This mode will help when the same delegate will be used repeatedly on multiple inputs. This is supported on non-iOS platforms. By default, it's disabled.
        --gpu_backend=                                  string  optional        Force the GPU delegate to use a particular backend for execution, and fail if unsuccessful. Should be one of: cl, gl
        --use_hexagon=false                             bool    optional        Use Hexagon delegate
        --hexagon_lib_path=/data/local/tmp              string  optional        The library path for the underlying Hexagon libraries. The library path ONLY for the libhexagon_nn_skel*.so files. For libhexagon_interface.so, it needs to be on a system library search path such as LD_LIBRARY_PATH.
        --hexagon_profiling=false                       bool    optional        Enables Hexagon profiling
```

#### Example:
```bash
adb shell "inf_diff_run_eval --model_file=<path to Models in device>/.tflite --delegate=xnnpack"
```
