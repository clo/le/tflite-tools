# TF Lite Tools

TF Lite tools can be used in Docker environment or installed on host file system

## TF Lite Docker

TF Lite tools can be used in docker environment. Details can be found in

[Docker Documentation](Docker.md)

## TF Lite Host

TF Lite tools can be installed on host file system. Details can be found in
[Host Documentation](Host.md)
